package cz.infi.javatravelagency.services;

import cz.infi.javatravelagency.dto.*;
import java.util.List;

/**
 *
 * @author Michael Musil
 */
public interface AdminService {

    /**
     * Assigns excursion to trip using only IDs of both entities
     * @param excursionId
     * @param tripId 
     */
    void assignExcursionToTrip(Long excursionId, Long tripId);

    /**
     * Creates new excursion from DTO
     * @param dto 
     */
    void createExcursion(ExcursionDTO dto);
 
    /**
     * Creates new trip from DTO
     * @param dto 
     */
    void createTrip(TripDTO dto);

    /**
     * Creates new customer from DTO
     * @param dto
     * @return 
     */
    CustomerDTO createCustomer(CustomerDTO dto);
    
    /**
     * Finds customer in database by ID and returns it's DTO.
     * @param id
     * @return 
     */
    CustomerDTO getCustomerById(Long id);
    
    /**
     * Finds excursion in database by ID and returns it's DTO.
     * @param id
     * @return 
     */
    ExcursionDTO getExcursionById(Long id);  
    
     /**
     * Finds trip in database by ID and returns it's DTO.
     * @param id
     * @return 
     */
    TripDTO getTripById(Long id);
    
    /**
     * Get list of all customers from database
     * @return 
     */
    List<CustomerDTO> listAllCustomers();
    
     /**
     * Get list of all excursions from database
     * @return 
     */
    List<ExcursionDTO> listAllExcursions();

    /**
     * Get list of all reservations from database
     * @return 
     */
    List<ReservationDTO> listAllReservations();

    /**
     * Get list of all trips from database
     * @return 
     */
    List<TripDTO> listAllTrips();

    /**
     * Removes customer from database
     * @param id 
     */
    void removeCustomer(Long id);
    
     /**
     * Removes excursion from database
     * @param id 
     */
    void removeExcursion(Long id);

    /**
     * Removes trip from database
     * @param id 
     */
    void removeTrip(Long id);

    /**
     * Updates excursion in database by DTO
     * @param dto 
     */
    void updateExcursion(ExcursionDTO dto);

    /**
     * Updates trip in database by DTO
     * @param dto 
     */
    void updateTrip(TripDTO dto);

    /**
     * Updates customer in database by DTO
     * @param dto 
     */
    void updateCustomer(CustomerDTO dto);
    
    /**
     * Removes reservation from database
     * @param id 
     */
    void removeReservation(Long id);
    
}
