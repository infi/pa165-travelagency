/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.infi.javatravelagency.services;

import cz.infi.javatravelagency.dto.CustomerDTO;

/**
 *
 * @author Michael Musil
 */
public interface PublicService {
    CustomerDTO createCustomer(CustomerDTO dto);    
}
