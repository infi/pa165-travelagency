package cz.infi.javatravelagency.services;

import cz.infi.javatravelagency.dto.ExcursionDTO;
import cz.infi.javatravelagency.dto.ReservationDTO;
import cz.infi.javatravelagency.dto.TripDTO;
import java.util.List;

/**
 *
 * @author infi
 */
public interface GuestService {

    /**
     * Adds excursion to reservation using IDs on both entities
     * @param reservationId
     * @param excursionId 
     */
    void addExcursionToReservation(Long reservationId, Long excursionId);

    /**
     * Finds available trips and returns it as list
     * @return 
     */
    List<TripDTO> listAvailableTrips();

    /**
     * Creates reservation from DTO
     * @param dto
     * @param username
     * @return 
     */
    ReservationDTO makeReservation(ReservationDTO dto, String username);

    /**
     * Finds trip by ID and returns it as DTO, it includes all assigned excursions
     * 
     * @param tripId
     * @return 
     */
    TripDTO tripDetail(Long tripId);
    
    /**
     * Finds reservation by ID and returns it as DTO, it includes all assigned excursions
     * @param id
     * @return 
     */
    ReservationDTO reservationDetail(Long id);

    /**
     * Finds all excursions
     * @return 
     */
    public List<ExcursionDTO> listExcursionsForTrip();
    
    /**
     * Cancels reservation
     * @return 
     */
    ReservationDTO cancelReservation(Long id);
}
