package cz.infi.javatravelagency.dto;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Libor
 */
public class ReservationDTO {
    private Long id;
    private Date date;
    private Long numberOfAttendants;
    private TripDTO trip;
    private List<ExcursionDTO> excursions;
    private boolean canceled;
    private CustomerDTO customer;

    public ReservationDTO() {
        canceled = false;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    public TripDTO getTrip() {
        return trip;
    }

    public void setTrip(TripDTO trip) {
        this.trip = trip;
    }

    public List<ExcursionDTO> getExcursions() {
        return excursions;
    }

    public void setExcursions(List<ExcursionDTO> excursions) {
        this.excursions = excursions;
    }

    public Long getNumberOfAttendants() {
        return numberOfAttendants;
    }

    public void setNumberOfAttendants(Long numberOfAttendants) {
        this.numberOfAttendants = numberOfAttendants;
    }

    /**
     * @return the canceled
     */
    public boolean isCanceled() {
        return canceled;
    }
    public boolean getCanceled() {
        return canceled;
    }
    /**
     * @param canceled the canceled to set
     */
    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }
    
    
}
