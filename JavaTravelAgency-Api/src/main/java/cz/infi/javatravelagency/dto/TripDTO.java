package cz.infi.javatravelagency.dto;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Libor
 */
public class TripDTO {

    private Long id;
    private Date dateFrom;
    private Date dateTo;
    private String destination;
    private Long availableTrips;
    private List<ExcursionDTO> excursions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }
   
    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }
    
    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
    
    public Long getAvailableTrips()
    {
        return availableTrips;
    }
    
    public void setAvailableTrips(Long availableTrips)
    {
        this.availableTrips = availableTrips;
    }
    
    public List<ExcursionDTO> getExcursions() {
        return excursions;
    }

    public void setExcursions(List<ExcursionDTO> excursions) {
        this.excursions = excursions;
    }
}
