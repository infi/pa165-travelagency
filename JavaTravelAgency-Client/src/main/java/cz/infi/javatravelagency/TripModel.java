package cz.infi.javatravelagency;

import cz.infi.javatravelagency.dto.TripDTO;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author infi
 */
public class TripModel {
    public static TripDTO GetById(int id) throws RestException {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target("http://localhost:8080/pa165/api");
        WebTarget resourceWebTarget = webTarget.path("trip/" + id);
        
        Invocation.Builder invocationBuilder =
            resourceWebTarget.request(MediaType.APPLICATION_JSON);
        
        Response response = invocationBuilder.get();

        TripDTO tripDTO;
        if(response.getStatus() == 200)
        {
            try {
                String json = response.readEntity(String.class);
                
                ObjectMapper mapper = new ObjectMapper();
                tripDTO = mapper.readValue(json, TripDTO.class);
                return tripDTO;
            } catch (Exception e) {
                throw new RestException("Other problem");
            }
        }
        else
        {
            throw new RestException("Status "+response.getStatus());
        }
    }

    static void Put(TripDTO trip) throws RestException {
        ObjectMapper mapper = new ObjectMapper();
        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        mapper.setDateFormat(df);
        
        Writer strWriter = new StringWriter();
        try {
            mapper.writeValue(strWriter, trip);
        } catch (IOException e) {
            throw new RestException("Other problem");
        }
        
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target("http://localhost:8080/pa165/api");
        WebTarget resourceWebTarget = webTarget.path("trip/" + trip.getId());
        
        Invocation.Builder invocationBuilder =
            resourceWebTarget.request(MediaType.APPLICATION_JSON);
            invocationBuilder.header("accept", "application/json");
            
            
        Response response = invocationBuilder.put(Entity.json(strWriter.toString()));
        if(response.getStatus() != 200)
        {
            throw new RestException("Status "+response.getStatus());
        }
            
       
    }
}
