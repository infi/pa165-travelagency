package cz.infi.javatravelagency;

import cz.infi.javatravelagency.dto.ExcursionDTO;
import cz.infi.javatravelagency.dto.TripDTO;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

/**
 *
 * @author Libor
 */
public class ClientApplication {
    
    
    public static HttpAuthenticationFeature feature;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        feature = HttpAuthenticationFeature.basic("rest", "rest");
        try
        {
            List argList = Arrays.asList(args);
            if (argList.contains("--help") || argList.contains("-h") || argList.isEmpty()) {
                System.out.println("Help for pa165-javaTravelAgency REST API client");
                System.out.println("You can use these arguments: ");
                System.out.println("--list-of-trips - print list of all trips");
                System.out.println("--test-trip-count - print count of trips");
                System.out.println("--add-example-trip - add example trip");
                System.out.println("--get-trip-by-id %id% - get trip by id, paramater id as number right after --get-trip-by-id");
                System.out.println("--rename-trip %id% %new_name% - rename trip");
                
                System.out.println("--list-of-excursions - print list of all trips");
                System.out.println("--test-excursion-count - print count of excursions");
                System.out.println("--add-example-excursion - add example excursion");
                System.out.println("--all - do all of tasks above (for demonstration purposes)");
            } 

            if (argList.contains("--list-of-trips")) {
                ListOfTrips(); 
            } 

            if (argList.contains("--test-trip-count")) {
                TestTripCount();
            }

            if (argList.contains("--add-example-trip")) {
                AddExampleTrip();
            }

            if (argList.contains("--get-trip-by-id")) {
                try
                {
                    int index = argList.indexOf("--get-trip-by-id");
                    if(argList.size() <= index + 1)
                    {
                        System.out.println("--get-trip-by-id requires as second parameter id");
                        return;
                    }
                    int number = Integer.parseInt(argList.get(index + 1).toString());
                    GetTripById(Integer.toString(number));
                }
                catch(NumberFormatException exception)
                {
                    System.out.println("--get-trip-by-id requires as second parameter id as number");
                }
            }

            if (argList.contains("--rename-trip")) {
                try
                {
                    int index = argList.indexOf("--rename-trip");
                    if(argList.size() <= index + 2)
                    {
                        System.out.println("--rename-trip requires as second parameter id and third parameter name");
                        return;
                    }
                    int number = Integer.parseInt(argList.get(index + 1).toString());
                    String name = argList.get(index + 2).toString();
                    RenameTrip(number,name);
                }
                catch(NumberFormatException exception)
                {
                    System.out.println("--rename-trip requires as second parameter id as number");
                }
            }
            
            if (argList.contains("--list-of-excursions")) {
                ListOfExcursions();
            } 

            if (argList.contains("--test-excursion-count")) {
                TestExcursionCount();
            }

            if (argList.contains("--add-example-excursion")) {
                AddExampleExcursion();
            }

            if (argList.contains("--all")) {
                ListOfTrips(); 
                TestTripCount();
                AddExampleTrip();
                GetTripById("1");
                GetTripById("100");
                ListOfExcursions();
                TestExcursionCount();
                AddExampleExcursion();
            }
        }
        catch(Exception exception)
        {
            System.out.println("We've run into problem with running client program");
        }
        
        
        
    }
    
    public static void ListOfTrips()
    {
        System.out.print("Getting list of trips: ");
        Client client = ClientBuilder.newClient();
        client.register(feature);
        WebTarget webTarget = client.target("http://localhost:8080/pa165/api");
        WebTarget resourceWebTarget = webTarget.path("trip");

        Invocation.Builder invocationBuilder =
        resourceWebTarget.request(MediaType.APPLICATION_JSON);
        invocationBuilder.header("accept", "application/json");

        try
        {
            Response response = invocationBuilder.get();
            if(response.getStatus() == 200)
            {
                System.out.print("[Success]");
                System.out.println(response.readEntity(String.class));
            }
            else
            {
                System.out.println("[Error] Problem with getting list of trips");
            }
        }
        catch(ProcessingException e)
        {
            System.out.println("There is a problem with connecting to REST API service.");
        }
        catch(Exception e)
        {
            System.out.println("Problem");
        }
    }
    
    public static void TestTripCount()
    {
        System.out.print("Getting trips count: ");
        Client client = ClientBuilder.newClient();
        client.register(feature);
        WebTarget webTarget = client.target("http://localhost:8080/pa165/api");
        WebTarget resourceWebTarget = webTarget.path("trip/count");
        
        Invocation.Builder invocationBuilder =
        resourceWebTarget.request(MediaType.TEXT_PLAIN);
        
        try
        {
            Response response = invocationBuilder.get();

            if(response.getStatus() == 200)
            {
                System.out.print("[Success] ");
                System.out.println(response.readEntity(String.class));
            }
            else
            {
                System.out.println("[Error] Problem with getting count of trips");
            }
        }
        catch(ProcessingException e)
        {
            System.out.println("There is a problem with connecting to REST API service.");
        }
        catch(Exception e)
        {
            System.out.println("Problem");
        }
    }
    
    public static void AddExampleTrip()
    {
        System.out.print("Adding example trip: ");
        TripDTO trip = new TripDTO();
        trip.setAvailableTrips(10L);
        Date today = new Date();
        Calendar c = Calendar.getInstance(); 
        c.setTime(today); 
        c.add(Calendar.DATE, 1);
        Date tomorrow = c.getTime();
        trip.setDateFrom(today);
        trip.setDateTo(tomorrow);
        trip.setDestination("Destination");
        
        ObjectMapper mapper = new ObjectMapper();
        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        mapper.setDateFormat(df);
        
        Writer strWriter = new StringWriter();
        try {
            mapper.writeValue(strWriter, trip);
        } catch (IOException e) {
            System.err.println("Error while converting entity to json");
        }
        
        Client client = ClientBuilder.newClient();
        client.register(feature);
        WebTarget webTarget = client.target("http://localhost:8080/pa165/api");
        WebTarget resourceWebTarget = webTarget.path("trip");
        
        Invocation.Builder invocationBuilder =
            resourceWebTarget.request(MediaType.APPLICATION_JSON);
            invocationBuilder.header("accept", "application/json");
        
            
        try
        {
            Response response = invocationBuilder.post(Entity.json(strWriter.toString()));
            if(response.getStatus() == 200)
            {
                System.out.print("[Success] ");
                System.out.println(response.readEntity(String.class));
            }
            else
            {
                System.out.println("[Error] Problem with adding example trip");
            }
            
        }
        catch(ProcessingException e)
        {
            System.out.println("There is a problem with connecting to REST API service.");
        }
        catch(Exception e)
        {
            System.out.println("Problem");
        }
    }
    
    public static void GetTripById(String id)
    {
        System.out.print("Getting trip by id(" + id  + "): ");
        Client client = ClientBuilder.newClient();
        client.register(feature);
        WebTarget webTarget = client.target("http://localhost:8080/pa165/api");
        WebTarget resourceWebTarget = webTarget.path("trip/" + id);
        
        Invocation.Builder invocationBuilder =
        resourceWebTarget.request(MediaType.APPLICATION_JSON);
        
        try
        {
            Response response = invocationBuilder.get();

            if(response.getStatus() == 200)
            {
                System.out.print("[Success] ");
                System.out.println(response.readEntity(String.class));
            }
            else if(response.getStatus() == 404)
            {
                System.out.println("[NotFound] Trip with id: " + id + " wasn't found");
            }
            else
            {
                System.out.println("[Error] Problem with getting trip by id");
            }
        }
        catch(ProcessingException e)
        {
            System.out.println("There is a problem with connecting to REST API service.");
        }
        catch(Exception e)
        {
            System.out.println("Problem");
        }
    }
         
    public static void RenameTrip(int id, String name)
    {
        System.out.println("Getting trip (" + id  + "): ");
        TripDTO trip;
        try {
            trip = TripModel.GetById(id);
            String originalName = trip.getDestination();
            System.out.println("Rename trip from " + originalName  + " to "+name+" ");
            trip.setDestination(name);
            TripModel.Put(trip);
            System.out.println("Trip renamed");
        } catch (RestException ex) {
            System.err.println(ex.getMessage());
        }
        
        
        
        
    }
    
    public static void ListOfExcursions()
    {
        System.out.print("Getting list of excursions: ");
        Client client = ClientBuilder.newClient();
        client.register(feature);
        WebTarget webTarget = client.target("http://localhost:8080/pa165/api");
        WebTarget resourceWebTarget = webTarget.path("excursion");
        
        Invocation.Builder invocationBuilder =
        resourceWebTarget.request(MediaType.APPLICATION_JSON);
        invocationBuilder.header("accept", "application/json");
        
        try
        {
            Response response = invocationBuilder.get();
            if(response.getStatus() == 200)
            {
                System.out.print("[Success] ");
                System.out.println(response.readEntity(String.class));
            }
            else
            {
                System.out.println("[Error] Problem with getting list of excursions");
            }
        }
        catch(ProcessingException e)
        {
            System.out.println("There is a problem with connecting to REST API service.");
        }
        catch(Exception e)
        {
            System.out.println("Problem");
        }
    }
    
    public static void TestExcursionCount()
    {
        System.out.print("Getting excursions count: ");
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target("http://localhost:8080/pa165/api");
        WebTarget resourceWebTarget = webTarget.path("excursion/count");
        
        Invocation.Builder invocationBuilder =
        resourceWebTarget.request(MediaType.TEXT_PLAIN);
        
        try
        {
            Response response = invocationBuilder.get();
            if(response.getStatus() == 200)
            {
                System.out.print("[Success] ");
                System.out.println(response.readEntity(String.class));
            }
            else
            {
                System.out.println("[Error] Problem with getting count of excursions");
            }
        }
        catch(ProcessingException e)
        {
            System.out.println("There is a problem with connecting to REST API service.");
        }
        catch(Exception e)
        {
            System.out.println("Problem");
        }
    }

    
    public static void AddExampleExcursion()
    {
        System.out.print("Adding example excursion: ");
        ExcursionDTO excursion = new ExcursionDTO();
        excursion.setDescription("Description");
        excursion.setDestination("Destination");
        excursion.setDuration(1);
        Date today = new Date();
        excursion.setExcursionDate(today);
        excursion.setPrice(500);

        ObjectMapper mapper = new ObjectMapper();
        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        mapper.setDateFormat(df);
        
        Writer strWriter = new StringWriter();
        try {
            mapper.writeValue(strWriter, excursion);
        } catch (IOException e) {
            System.err.println("Error while converting entity to json");
        }
        
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target("http://localhost:8080/pa165/api");
        WebTarget resourceWebTarget = webTarget.path("excursion");
        
        Invocation.Builder invocationBuilder =
            resourceWebTarget.request(MediaType.APPLICATION_JSON);
            invocationBuilder.header("accept", "application/json");
        try
        {
            Response response = invocationBuilder.post(Entity.json(strWriter.toString()));
            if(response.getStatus() == 200)
            {
                System.out.print("[Success] ");
                System.out.println(response.readEntity(String.class));
            }
            else
            {
                System.out.println("[Error] Problem with adding example excursion");
            }
        }
        catch(ProcessingException e)
        {
            System.out.println("There is a problem with connecting to REST API service.");
        }
        catch(Exception e)
        {
            System.out.println("Problem");
        }
    }
}
