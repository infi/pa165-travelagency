package cz.infi.javatravelagency;


/**
 *
 * @author Libor
 */
public class RestException extends Exception {

    public RestException(String message) {
        super("ERROR - remote resource failed - " + message);
    }

}
