package cz.infi.travelagency.services;

import cz.infi.javatravelagency.dao.CustomerDAO;
import cz.infi.javatravelagency.dao.ExcursionDAO;
import cz.infi.javatravelagency.dao.ReservationDAO;
import cz.infi.javatravelagency.dao.TripDAO;
import cz.infi.javatravelagency.dto.ReservationDTO;
import cz.infi.javatravelagency.dto.TripDTO;
import cz.infi.javatravelagency.entities.Reservation;
import cz.infi.javatravelagency.entities.Trip;
import cz.infi.javatravelagency.services.DtoConverterImpl;
import cz.infi.javatravelagency.services.GuestServiceImpl;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Libor
 */
public class GuestServiceTest extends AbstractUnitTest {
    
    private ReservationDAO reservationDAO;
    private CustomerDAO customerDAO;
    private TripDAO tripDAO;
    private ExcursionDAO excursionDAO;
    private DtoConverterImpl converter;
    
    @Autowired
    private GuestServiceImpl guestServices;
    
    @Before
    public void doSetup() {
        reservationDAO = mock(ReservationDAO.class);
        customerDAO = mock(CustomerDAO.class);
        tripDAO = mock(TripDAO.class);
        excursionDAO = mock(ExcursionDAO.class);
        
        converter = new DtoConverterImpl();
   
        
        Trip trip1 = new Trip();
        trip1.setId(new Long(1));
        
        Trip trip2 = new Trip();
        trip2.setId(new Long(2));
        
        List<Trip> trips = new ArrayList<>();
        trips.add(trip1);
        trips.add(trip2);
        
        when(tripDAO.listAll()).thenReturn(trips);
        when(tripDAO.listAvailableTrips()).thenReturn(trips);
        
        guestServices.setCustomerDAO(customerDAO);
        guestServices.setReservationDAO(reservationDAO);
        guestServices.setTripDAO(tripDAO);
        guestServices.setExcursionDAO(excursionDAO);
              
    }
    
    @Test
    public void testListAllAvailableTripsWasCalled()
    {
        
        List<TripDTO> trips = guestServices.listAvailableTrips();
        
        verify(tripDAO).listAvailableTrips();
    }
    
    @Test
    public void testListAllAvailableTrips()
    {        
        List<TripDTO> trips = guestServices.listAvailableTrips();
        
        assertEquals(trips.get(0).getId(), new Long(1));
        assertEquals(trips.get(1).getId(), new Long(2));
    }
    
    @Test
    public void testMakeReservation()
    {
        ReservationDTO reservation = new ReservationDTO();
        Reservation result = converter.dtoToEntity(reservation);
        reservation.setTrip(new TripDTO());
        
        when(reservationDAO.persist(result)).thenReturn(result);
        
        guestServices.makeReservation(reservation,"");
        
        verify(reservationDAO).persist(result);
    }
    
    @Test
    public void testDetailTrip()
    {
        Trip trip1 = new Trip();
        Long id = 1L;
        trip1.setId(id);
        
        when(tripDAO.findById(id)).thenReturn(trip1);
        
        TripDTO result = guestServices.tripDetail(id);
        
        assertEquals(trip1.getId(), result.getId());
    }
}
