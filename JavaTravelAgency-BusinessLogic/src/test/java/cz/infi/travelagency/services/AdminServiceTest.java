/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.infi.travelagency.services;
import cz.infi.javatravelagency.dao.CustomerDAO;
import cz.infi.javatravelagency.dao.ExcursionDAO;
import cz.infi.javatravelagency.dao.ReservationDAO;
import cz.infi.javatravelagency.dao.TripDAO;
import cz.infi.javatravelagency.dto.CustomerDTO;
import cz.infi.javatravelagency.dto.ExcursionDTO;
import cz.infi.javatravelagency.dto.ReservationDTO;
import cz.infi.javatravelagency.dto.TripDTO;
import cz.infi.javatravelagency.entities.Customer;
import cz.infi.javatravelagency.entities.Excursion;
import cz.infi.javatravelagency.entities.Reservation;
import cz.infi.javatravelagency.entities.Trip;
import cz.infi.javatravelagency.services.AdminServiceImpl;
import cz.infi.javatravelagency.services.DtoConverterImpl;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;
/**
 *
 * @author Libor
 */
public class AdminServiceTest extends AbstractUnitTest {
    
    private ReservationDAO reservationDAO;
    private CustomerDAO customerDAO;
    private TripDAO tripDAO;
    private ExcursionDAO excursionDAO;
    private DtoConverterImpl converter;
    
    @Autowired
    private AdminServiceImpl adminServices;
    
    @Before
    public void doSetup() {
        reservationDAO = mock(ReservationDAO.class);
        customerDAO = mock(CustomerDAO.class);
        tripDAO = mock(TripDAO.class);
        excursionDAO = mock(ExcursionDAO.class);
        
        converter = new DtoConverterImpl();
        
        Customer customer1 = new Customer();
        customer1.setId(new Long(1));
        Customer customer2 = new Customer();
        customer2.setId(new Long(2));
        
        List<Customer> customers = new ArrayList<>();
        customers.add(customer1);
        customers.add(customer2);

        when(customerDAO.listAll()).thenReturn(customers);
        
        Reservation reservation1 = new Reservation();
        reservation1.setId(new Long(1));
        Reservation reservation2 = new Reservation();
        reservation2.setId(new Long(2));
        
        List<Reservation> reservations = new ArrayList<>();
        reservations.add(reservation1);
        reservations.add(reservation2);
        
        when(reservationDAO.listAll()).thenReturn(reservations);
        
        Trip trip1 = new Trip();
        trip1.setId(new Long(1));
        Trip trip2 = new Trip();
        trip2.setId(new Long(2));
        
        List<Trip> trips = new ArrayList<>();
        trips.add(trip1);
        trips.add(trip2);
        
        when(tripDAO.listAll()).thenReturn(trips);
        
        adminServices.setCustomerDAO(customerDAO);
        adminServices.setReservationDAO(reservationDAO);
        adminServices.setTripDAO(tripDAO);
        adminServices.setExcursionDAO(excursionDAO);
        
        
    }
    
    @Test
    public void testListAllReservationsWasCalled()
    {
        
        List<ReservationDTO> results = adminServices.listAllReservations();
        
        verify(reservationDAO).listAll();
    }
    
    @Test
    public void testListAllReservations()
    {        
        List<ReservationDTO> results = adminServices.listAllReservations();
        
        assertEquals(results.get(0).getId(), new Long(1));
        assertEquals(results.get(1).getId(), new Long(2));
    }
    
    @Test
    public void testListAllCustomersWasCalled()
    {
        List<CustomerDTO> customers = adminServices.listAllCustomers();
        
        verify(customerDAO).listAll();
    }
    
    @Test
    public void testListAllCustomers()
    {
        List<CustomerDTO> customers = adminServices.listAllCustomers();
        
        assertEquals(customers.get(0).getId(), new Long(1));
        assertEquals(customers.get(1).getId(), new Long(2));
    }
    
        @Test
    public void testListAllTripsWasCalled()
    {
        List<TripDTO> trips = adminServices.listAllTrips();
        
        verify(tripDAO).listAll();
    }
    
    @Test
    public void testListAllTrips()
    {
        List<TripDTO> trips = adminServices.listAllTrips();
        
        assertEquals(trips.get(0).getId(), new Long(1));
        assertEquals(trips.get(1).getId(), new Long(2));
    }
    
    @Test
    public void testCreateTripPersistWasCalled()
    {
        TripDTO trip = new TripDTO();
        Trip result = converter.dtoToEntity(trip);
        
        adminServices.createTrip(trip);
        
        verify(tripDAO).persist(result);
    }
    
    @Test
    public void testUpdateTripWasCalled()
    {
        TripDTO trip = new TripDTO();
        Trip result = converter.dtoToEntity(trip);
        
        adminServices.updateTrip(trip);
        
        verify(tripDAO).update(result);
    }
    
    @Test
    public void testRemoveTripWasCalled()
    {
        Trip trip = new Trip();
        Long id = new Long(1);
        trip.setId(id);  
        
        when(tripDAO.findById(id)).thenReturn(trip);
        
        adminServices.removeTrip(id);
        
        verify(tripDAO).remove(trip);
    }
    
    @Test
    public void testCreateExcursionPersistWasCalled()
    {
        ExcursionDTO excursion = new ExcursionDTO();
        Excursion result = converter.dtoToEntity(excursion);
        
        adminServices.createExcursion(excursion);
        
        verify(excursionDAO).persist(result);
    }
    
    @Test
    public void testUpdateExcursionWasCalled()
    {
        ExcursionDTO excursion = new ExcursionDTO();
        Excursion result = converter.dtoToEntity(excursion);
        
        adminServices.updateExcursion(excursion);
        
        verify(tripDAO).update(result);
    }
    
    @Test
    public void testRemoveExcursionWasCalled()
    {
        Excursion excursion = new Excursion();
        Long id = new Long(1);
        excursion.setId(id);
           
        when(excursionDAO.findById(id)).thenReturn(excursion);
        
        adminServices.removeExcursion(id);
        
        verify(excursionDAO).remove(excursion);
    }
    
    @Test
    public void testRemoveCustomer()
    {
        Customer customer = new Customer();
        Long id = new Long(1);
        customer.setId(id);
           
        when(customerDAO.findById(id)).thenReturn(customer);
        
        adminServices.removeCustomer(id);
        
        verify(customerDAO).remove(customer);
    }
}
