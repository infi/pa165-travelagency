package cz.infi.travelagency.dao;

import cz.infi.javatravelagency.entities.Excursion;
import cz.infi.javatravelagency.dao.ExcursionDAO;

import java.util.Calendar;
import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertEquals;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Libor
 */
public class ExcursionDAOTest extends AbstractDAOTest{

    @Autowired
    private ExcursionDAO excursionDAO;
        
    @Test
    public void testPersistExcursion() {
        Calendar c = Calendar.getInstance();
        c.set(2013, 8, 24);
        Excursion ex1 = new Excursion(c.getTime(), 10, "Simple excursion 1", "New York", 35);
        ex1 = excursionDAO.persist(ex1);
        
        assertEquals(excursionDAO.findById(ex1.getId()), ex1);
    }

    @Test
    public void testRemoveExcursion() {
        Calendar c = Calendar.getInstance();
        c.set(2013, 8, 24);
        Excursion ex1 = new Excursion(c.getTime(), 10, "Simple excursion 1", "New York", 35);
        ex1 = excursionDAO.persist(ex1);
        excursionDAO.remove(ex1);
        
        assertNull(excursionDAO.findById(ex1.getId()));
    }
    
    @Test
    public void testUpdateExcursion() {
        Calendar c = Calendar.getInstance();
        c.set(2013, 8, 24);
        Excursion ex1 = new Excursion(c.getTime(), 10, "Simple excursion 1", "New York", 35);
        ex1 = excursionDAO.persist(ex1);
        ex1.setDestination("Prague");
        assertEquals(excursionDAO.update(ex1), ex1);
    }
    
}
