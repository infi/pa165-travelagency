package cz.infi.travelagency.dao;

import cz.infi.javatravelagency.entities.Excursion;
import cz.infi.javatravelagency.dao.ExcursionDAO;
import cz.infi.javatravelagency.dao.ReservationDAO;
import cz.infi.javatravelagency.dao.TripDAO;
import cz.infi.javatravelagency.entities.Reservation;
import cz.infi.javatravelagency.entities.Trip;
import java.util.Arrays;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import static junit.framework.Assert.assertTrue;
import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertEquals;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Miloš Skoták
 */
public class ReservationDAOTest extends AbstractDAOTest {    
    
    @Autowired
    private TripDAO tripDAO;
    
    @Autowired
    private ReservationDAO reservationDAO;    
   
    @Autowired
    private ExcursionDAO excursionDAO;
        
    @Test
    public void testAddingReservation(){
        Calendar c1= Calendar.getInstance();
        Calendar c2= Calendar.getInstance();
        c1.set(2013, 8, 24);
        c2.set(2013, 8, 29);
        
        c1.set(2013, 8, 24);
        Excursion ex1 = createExcursion(c1.getTime(),10,"Simple excursion 1","New York",35);; 
        c1.set(2014, 5, 25);
        Excursion ex2=createExcursion(c1.getTime(),11,"Simple excursion 2","Cairo",50);

        ex1 = excursionDAO.persist(ex1);
        ex2 = excursionDAO.persist(ex2);

        Excursion [] field1={ex1,ex2};
        
        c1.set(2015, 1, 20);
        c2.set(2015, 8, 21);
        Trip tr1 = createTrip(c1.getTime(),c2.getTime(),"USA California",4L,Arrays.asList(field1));
        tr1 = tripDAO.persist(tr1);
                
        Trip [] tripList1 ={tr1};
        
        Reservation r1=createReservation(tr1);
        r1 = reservationDAO.persist(r1);
        assertEquals(reservationDAO.findById(r1.getId()), r1);
    }
    
    @Test
    public void testUpdateReservation(){
        Calendar c1= Calendar.getInstance();
        Calendar c2= Calendar.getInstance();
        c1.set(2013, 8, 24);
        c2.set(2013, 8, 29);
          
        c1.set(2013, 8, 24);
        Excursion ex1 = createExcursion(c1.getTime(),10,"Simple excursion 1","New York",35);; 
        c1.set(2014, 5, 25);
        Excursion ex2=createExcursion(c1.getTime(),11,"Simple excursion 2","Cairo",50);
        c1.set(2013, 8, 27);
        Excursion ex3=createExcursion(c1.getTime(),12,"Simple excursion 3","Boston",40);
        c1.set(2013, 8, 27);
        Excursion ex4=createExcursion(c1.getTime(),8,"Simple excursion 4","Alexandria",15);

        ex1 = excursionDAO.persist(ex1);
        ex2 = excursionDAO.persist(ex2);
        ex3 = excursionDAO.persist(ex3);
        ex4 = excursionDAO.persist(ex4);
        Excursion [] field1={ex1,ex3};
        Excursion [] field2={ex2,ex4};
        Trip tr1 = createTrip(c1.getTime(),c2.getTime(),"USA California",4L,Arrays.asList(field1));
        tr1 = tripDAO.persist(tr1);
        
        c1.set(2014, 4, 10);
        c2.set(2014, 6, 11);
        Trip tr2 = createTrip(c1.getTime(),c2.getTime(),"Egypt",8L,Arrays.asList(field2));
        tr2 = tripDAO.persist(tr2);
        
        Excursion [] field3 = {};
        c1.set(2015, 2, 11);
        c2.set(2015, 5, 13);
        Trip tr3 = createTrip(c1.getTime(),c2.getTime(),"Costa Rica",12L,Arrays.asList(field3));
        tr3 = tripDAO.persist(tr3);
        
        Trip [] tripList1 ={tr1};
        Trip [] tripList2 = {tr2};
        
        Reservation r1=createReservation(tr1);
        r1 = reservationDAO.persist(r1);
        r1.setTrip(tr1);
        assertEquals(reservationDAO.findById(r1.getId()), r1);
    }
    
    @Test
    public void testRemoveReservation(){
        Calendar c1= Calendar.getInstance();
        Calendar c2= Calendar.getInstance();
        c1.set(2013, 8, 24);
        c2.set(2013, 8, 29);
        
        c1.set(2013, 8, 24);
        Excursion ex1 = createExcursion(c1.getTime(),10,"Simple excursion 1","New York",35);; 
        c1.set(2014, 5, 25);
        Excursion ex2=createExcursion(c1.getTime(),11,"Simple excursion 2","Cairo",50);

        ex1 = excursionDAO.persist(ex1);
        ex2 = excursionDAO.persist(ex2);

        Excursion [] field1={ex1,ex2};
        
        c1.set(2015, 1, 20);
        c2.set(2015, 8, 21);
        Trip tr1 = createTrip(c1.getTime(),c2.getTime(),"USA California",4L,Arrays.asList(field1));
        tr1 = tripDAO.persist(tr1);
                
        Trip [] tripList1 ={tr1};
        
        Reservation r1=createReservation(tr1);
        r1 = reservationDAO.persist(r1);
        
        reservationDAO.remove(r1);
        assertNull(reservationDAO.findById(r1.getId()));
    }
    
    @Test
    public void testListingOfReservation(){
        Calendar c1= Calendar.getInstance();
        Calendar c2= Calendar.getInstance();
        c1.set(2013, 8, 24);
        c2.set(2013, 8, 29);
        
        c1.set(2013, 8, 24);
        Excursion ex1 = createExcursion(c1.getTime(),10,"Simple excursion 1","New York",35);; 
        c1.set(2014, 5, 25);
        Excursion ex2=createExcursion(c1.getTime(),11,"Simple excursion 2","Cairo",50);

        ex1 = excursionDAO.persist(ex1);
        ex2 = excursionDAO.persist(ex2);

        Excursion [] field1={ex1,ex2};
        
        c1.set(2015, 1, 20);
        c2.set(2015, 8, 21);
        Trip tr1 = createTrip(c1.getTime(),c2.getTime(),"USA California",4L,Arrays.asList(field1));
        tr1 = tripDAO.persist(tr1);
                
        Trip [] tripList1 ={tr1};
        
        Reservation r1=createReservation(tr1);
        r1 = reservationDAO.persist(r1);

        List<Reservation> l1 = reservationDAO.listAll();
        assertTrue(l1.contains(r1));

    }
    

    
    public Trip createTrip(Date from,Date to,String destination,Long avTrips,List<Excursion> excursions){
        Trip trip=new Trip();
        trip.setAvailableTrips(avTrips);
        trip.setDestination(destination);
        trip.setDateFrom(from);
        trip.setDateTo(to);
        trip.setExcursions(excursions);
        return trip;
    }
    
    public Excursion createExcursion(Date excursionDate,
        int duration,String description,String destination,int price){
        Excursion ex = new Excursion();
        ex.setDescription(description);
        ex.setDestination(destination);
        ex.setDuration(duration);
        ex.setExcursionDate(excursionDate);
        ex.setPrice(price);
        return ex;   
    }

    private Reservation createReservation(Trip trip){
        Reservation temp = new Reservation();
        temp.setTrip(trip);
        return temp;
    }
    
    
}
