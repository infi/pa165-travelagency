package cz.infi.travelagency.dao;

import cz.infi.javatravelagency.entities.Excursion;
import cz.infi.javatravelagency.entities.Trip;
import cz.infi.javatravelagency.dao.ExcursionDAO;
import cz.infi.javatravelagency.dao.TripDAO;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Micheal Musil
 */
public class TripDAOTest extends AbstractDAOTest {
    
    
    @Autowired
    private TripDAO tripDAO;

    @Autowired
    private ExcursionDAO excursionDAO;
    
    private Calendar calendar;
    
    @Before
    public void setUp() {
        calendar = Calendar.getInstance();
    }
        
    @Test
    public void testPersistSimpleTrip() {
        Trip trip = createSimpleSampleTrip();
        
        Long id = this.persistTrip(trip);
        assertEquals("Created",tripDAO.findById(id), trip);

    }   
    
    @Transactional
    private Long persistTrip(Trip trip) {
        tripDAO.persist(trip);
        return trip.getId();
    }
    
    @Test
    public void testAddExcursion() {
        Trip trip = createSimpleSampleTrip();
        
        Excursion ex1 = createExcursion(calendar.getTime(),10,"Simple excursion 1","New York",35);
        
        Excursion [] excursions = {ex1};
        trip.setExcursions(Arrays.asList(excursions));
        
        //persist Excursion
        excursionDAO.persist(ex1);
        
        tripDAO.persist(trip);
        
        List<Excursion> excursionsOut = trip.getExcursions();
        
        assertEquals("Get same excursion as saved",excursionsOut.get(0),ex1);
    }
    
    @Test
    public void testRemoveTrip() {
        Trip trip = createSimpleSampleTrip();
        trip = tripDAO.persist(trip);
        Long id = trip.getId();
        
        tripDAO.remove(trip);
        assertNull(tripDAO.findById(id));    
    }
  
    @Test
    public void testUpdateTrip() {
        Trip trip = createSimpleSampleTrip();
        trip = tripDAO.persist(trip);
         
        tripDAO.persist(trip);
        
        assertEquals(trip.getDestination(),"Iceland");
        
        trip.setDestination("Norway");
        Trip trip2 = tripDAO.update(trip);
       
        
        assertEquals(trip.getDestination(),"Norway");
    }    
    
    @Test
    public void testComplexTrip(){
        
        Calendar c1= Calendar.getInstance();
        Calendar c2= Calendar.getInstance();
        c1.set(2013, 8, 24);
        c2.set(2013, 8, 29);
        
        
        c1.set(2013, 8, 24);
        Excursion ex1 = createExcursion(c1.getTime(),10,"Simple excursion 1","New York",35);
        c1.set(2014, 5, 25);
        Excursion ex2=createExcursion(c1.getTime(),11,"Simple excursion 2","Cairo",50);
        c1.set(2013, 8, 27);
        Excursion ex3=createExcursion(c1.getTime(),12,"Simple excursion 3","Boston",40);
        c1.set(2013, 8, 27);
        Excursion ex4=createExcursion(c1.getTime(),8,"Simple excursion 4","Alexandria",15);

        ex1 = excursionDAO.persist(ex1);
        ex2 = excursionDAO.persist(ex2);
        ex3 = excursionDAO.persist(ex3);
        ex4 = excursionDAO.persist(ex4);
        Excursion [] field1={ex1,ex3};
        Excursion [] field2={ex2,ex4};
        Trip tr1 = createTrip(c1.getTime(),c2.getTime(),"USA California",4L,Arrays.asList(field1));
        tr1 = tripDAO.persist(tr1);
        assertEquals(tripDAO.findById(tr1.getId()), tr1);
        
        c1.set(2014, 4, 10);
        c2.set(2014, 6, 11);
        Trip tr2 = createTrip(c1.getTime(),c2.getTime(),"Egypt",8L,Arrays.asList(field2));
        tr2 = tripDAO.persist(tr2);
        assertEquals(tripDAO.findById(tr2.getId()), tr2);
        
        tripDAO.remove(tr1);
        assertNull(tripDAO.findById(tr1.getId()));
        assertFalse(tripDAO.listAllExcursions().contains(ex1));
        assertFalse(tripDAO.listAllExcursions().contains(ex3));
    }
    
    @Test
    public void testListAvailableTrips() {
        calendar.set(2013, 4, 2);
        Date from = calendar.getTime();
        calendar.set(2013, 4, 5);
        Date to = calendar.getTime(); 
        Excursion [] field={};
        
        Trip trip = createTrip(from,to,"test",10L,Arrays.asList(field));
        tripDAO.persist(trip);
        
        assertEquals(tripDAO.listAvailableTrips().size(), 1);
        
        trip = createTrip(from,to,"test",0L,Arrays.asList(field));
        tripDAO.persist(trip);
        
        assertEquals(tripDAO.listAvailableTrips().size(), 1);

        trip = createTrip(from,to,"test",10L,Arrays.asList(field));
        tripDAO.persist(trip);
        
        assertEquals(tripDAO.listAvailableTrips().size(), 2);
    }    
    
    public Trip createSimpleSampleTrip() {
        Trip trip=new Trip();
        trip.setAvailableTrips(10L);
        trip.setDestination("Iceland");
        calendar.set(2013, 4, 2);
        trip.setDateFrom(calendar.getTime());
        calendar.set(2013, 4, 4);
        trip.setDateTo(calendar.getTime());
        return trip;
    }
    
    public Trip createTrip(Date from,Date to,String destination,Long avTrips,List<Excursion> excursions){
        Trip trip=new Trip();
        trip.setAvailableTrips(avTrips);
        trip.setDestination(destination);
        trip.setDateFrom(from);
        trip.setDateTo(to);
        trip.setExcursions(excursions);
      return trip;
    }
    
    public Excursion createExcursion(Date excursionDate,
            int duration,String description,String destination,int price){
        Excursion ex = new Excursion();
        ex.setDescription(description);
        ex.setDestination(destination);
        ex.setDuration(duration);
        ex.setExcursionDate(excursionDate);
        ex.setPrice(price);
     return ex;   
    }
   
}
