package cz.infi.travelagency.dao;


import java.sql.SQLException;
import java.util.logging.Logger;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author infi
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:applicationContextIntegrationTest.xml"})
@TransactionConfiguration(defaultRollback = true)
@Transactional
public abstract class AbstractDAOTest {
    protected static Logger logger = Logger.getLogger(ExcursionDAOTest.class
            .getName());


    
    @Before
    public void setUp() throws SQLException, ClassNotFoundException {
        logger.info("Starting in-memory database for unit tests");
    }
}
