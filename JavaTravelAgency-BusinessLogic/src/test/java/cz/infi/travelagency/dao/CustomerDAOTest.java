package cz.infi.travelagency.dao;

import cz.infi.javatravelagency.entities.Customer;
import cz.infi.javatravelagency.entities.Reservation;
import cz.infi.javatravelagency.dao.CustomerDAO;
import java.util.Arrays;
import java.util.List;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
/**
 *
 * @author Ondrej Fujtik
 */
public class CustomerDAOTest extends AbstractDAOTest {
    
    @Autowired
    private CustomerDAO customerDAO;
    
    @Test
    public void testUpdatingCustomer(){
        Reservation [] rfield1={};
        Customer customer=createCustomer("Matt","Damon","MattDamon@gmail.com",Arrays.asList(rfield1));
        customer = customerDAO.persist(customer);
        assertEquals(customerDAO.findById(customer.getId()), customer);
        customer.setName("Bill");
        customerDAO.update(customer);
        assertEquals(customerDAO.findById(customer.getId()), customer);
    }
    
    @Test
    public void testAddingUser(){
        Reservation [] rfield1={};
        Customer customer=createCustomer("Bob","Marley","BobMarley@gmail.com",Arrays.asList(rfield1));
        customer = customerDAO.persist(customer);
        assertEquals(customerDAO.findById(customer.getId()), customer);
    }
    
    @Test
    public void testRemovingUser(){
        Reservation [] rfield2={};
        Customer cust2=createCustomer("John","Travolta","JohnTravolta@gmail.com",Arrays.asList(rfield2));
        cust2 = customerDAO.persist(cust2);
        customerDAO.remove(cust2);
        assertNull(customerDAO.findById(cust2.getId()));
    }
    
    @Test
    public void testListingOfCustomers(){
        Reservation [] rfield2={};
        Customer cust2=createCustomer("John","Travolta","JohnTravolta@gmail.com",Arrays.asList(rfield2));
        Reservation [] rfield3={};
        Customer cust3=createCustomer("Michael","Douglas","MichaelDouglas@gmail.com",Arrays.asList(rfield3));
        cust3 = customerDAO.persist(cust3);
        cust2 = customerDAO.persist(cust2);
        List<Customer> l1 = customerDAO.listAll();
        assertTrue(l1.contains(cust2));
        assertTrue(l1.contains(cust3));
    }
    
    private Customer createCustomer(String name,String surname,String contacts,List<Reservation> reservationList){
        Customer temp = new Customer();
        temp.setName(name);
        temp.setSurname(surname);
        temp.setContacts(contacts);
        temp.setReservations(reservationList);
        return temp;
    }
    
}

