package cz.infi.javatravelagency.services;

import cz.infi.javatravelagency.entities.Customer;
import cz.infi.javatravelagency.entities.Excursion;
import cz.infi.javatravelagency.entities.Reservation;
import cz.infi.javatravelagency.entities.Trip;
import cz.infi.javatravelagency.dto.*;
import org.springframework.stereotype.Service;

/**
 *
 * @author infi
 */
@Service
public class DtoConverterImpl implements DtoConverter {
    
    @Override
    public Trip dtoToEntity(TripDTO dto) {
        Trip entity = new Trip();
        entity.setId(dto.getId());
        entity.setDestination( dto.getDestination() );
        entity.setDateFrom( dto.getDateFrom() );
        entity.setDateTo( dto.getDateTo() );
        entity.setAvailableTrips( dto.getAvailableTrips() );
        return entity;
    }
    
    @Override
    public TripDTO entityToDto(Trip entity) {
        TripDTO dto = new TripDTO();
        dto.setId(entity.getId());
        dto.setDestination( entity.getDestination() );
        dto.setDateFrom( entity.getDateFrom() );
        dto.setDateTo( entity.getDateTo() );
        dto.setAvailableTrips( entity.getAvailableTrips() );
        return dto;        
    }
    
    @Override
    public Reservation dtoToEntity(ReservationDTO dto) {
        Reservation entity = new Reservation();
        entity.setId ( dto.getId() );
        entity.setDate( dto.getDate() );
        entity.setNumberOfAttendants( dto.getNumberOfAttendants() );
        entity.setCanceled( dto.isCanceled() );
        return entity;
    }

    @Override
    public ReservationDTO entityToDto(Reservation entity) {
        ReservationDTO dto = new ReservationDTO();
        dto.setId( entity.getId() );
        dto.setDate( entity.getDate() );
        dto.setNumberOfAttendants( entity.getNumberOfAttendants() );
        dto.setCanceled( entity.isCanceled() );
        return dto;
    }
    
    @Override
    public Excursion dtoToEntity(ExcursionDTO dto) {
        Excursion entity = new Excursion();
        entity.setId( dto.getId() );
        entity.setDescription( dto.getDescription() );
        entity.setDestination( dto.getDestination() );
        entity.setDuration( dto.getDuration() );
        entity.setExcursionDate( dto.getExcursionDate() );
        entity.setPrice( dto.getPrice() );
        return entity;
    }    

    @Override
    public ExcursionDTO entityToDto(Excursion entity) {
        ExcursionDTO dto = new ExcursionDTO();
        dto.setId( entity.getId() );
        dto.setDescription( entity.getDescription() );
        dto.setDestination( entity.getDestination() );
        dto.setDuration( entity.getDuration() );
        dto.setExcursionDate( entity.getExcursionDate() );
        dto.setPrice( entity.getPrice() );
        return dto;
    }     
    
    @Override
    public Customer dtoToEntity(CustomerDTO dto) {
        Customer entity = new Customer();
        entity.setId( dto.getId() );
        entity.setContacts( dto.getContacts() );
        entity.setName( dto.getName() );
        entity.setSurname( dto.getSurname() );
        entity.setUsername( dto.getUsername() );
        entity.setPassword( dto.getPassword() );
        entity.setUserrole( dto.getUserrole() );
        entity.setAddress( dto.getAddress() );
        entity.setPhone( dto.getPhone() );
        return entity;
    }
    
    @Override
    public CustomerDTO entityToDto(Customer entity) {
        CustomerDTO dto = new CustomerDTO();
        dto.setId( entity.getId() );
        dto.setContacts( entity.getContacts() );
        dto.setName( entity.getName() );
        dto.setSurname( entity.getSurname() );
        dto.setUsername( entity.getUsername() );
        dto.setUserrole( entity.getUserrole() );
        dto.setAddress( entity.getAddress() );
        dto.setPhone( entity.getPhone() );
        return dto;
    }    
        
}
