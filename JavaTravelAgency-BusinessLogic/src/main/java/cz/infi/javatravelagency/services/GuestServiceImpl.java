package cz.infi.javatravelagency.services;

import cz.infi.javatravelagency.dao.CustomerDAO;
import cz.infi.javatravelagency.dao.ExcursionDAO;
import cz.infi.javatravelagency.dao.ReservationDAO;
import cz.infi.javatravelagency.dao.TripDAO;
import cz.infi.javatravelagency.entities.Excursion;
import cz.infi.javatravelagency.entities.Trip;
import cz.infi.javatravelagency.dto.ExcursionDTO;
import cz.infi.javatravelagency.dto.ReservationDTO;
import cz.infi.javatravelagency.dto.TripDTO;
import cz.infi.javatravelagency.entities.Customer;
import cz.infi.javatravelagency.entities.Reservation;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author infi
 */
public class GuestServiceImpl implements GuestService {
    
    private ReservationDAO reservationDAO;
    public void setReservationDAO(ReservationDAO reservationDAO) {
        this.reservationDAO = reservationDAO;
    }

    private CustomerDAO customerDAO; 
    public void setCustomerDAO(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }    
    
    private TripDAO tripDAO;
    public void setTripDAO(TripDAO tripDAO) {
        this.tripDAO = tripDAO;
    } 
  
    private ExcursionDAO excursionDAO;
    public void setExcursionDAO(ExcursionDAO excursionDAO) {
        this.excursionDAO = excursionDAO;
    } 
    
    private DtoConverterImpl dtoConverter;
    @Required  
    public void setDtoConverter(DtoConverterImpl dtoConverter) {
        this.dtoConverter = dtoConverter;
    }     

    @Override
    @Transactional 
    public List<TripDTO> listAvailableTrips() {
        List<Trip> tripList= tripDAO.listAvailableTrips();
        List<TripDTO> dtoList = new ArrayList<>();
        for (Trip trip : tripList) {
            TripDTO tripDTO = dtoConverter.entityToDto(trip);
            dtoList.add(tripDTO);
        }
        return dtoList;
    }    
    
    /**
     * Trip detail with all available excursions
     */
    @Override
    @Transactional 
    public TripDTO tripDetail(Long tripId) {
        Trip trip = tripDAO.findById(tripId);
        TripDTO tripDTO = dtoConverter.entityToDto(trip);
        
        List<ExcursionDTO> dtoList = new ArrayList<>();
        if (trip.getExcursions() != null) {
            for (Excursion excursion : trip.getExcursions()) {
                dtoList.add(dtoConverter.entityToDto(excursion));
            }
        }

        tripDTO.setExcursions(dtoList);
        
        return tripDTO;
    } 
    
    /**
     * Reservation detail with all available excursions
     */
    @Override
    @Transactional 
    public ReservationDTO reservationDetail(Long id) {
        Reservation reservation = reservationDAO.findById(id);
        ReservationDTO reservationDTO = dtoConverter.entityToDto(reservation);
        
        List<ExcursionDTO> dtoList = new ArrayList<>();
        if (reservation.getExcursions() != null) {
            for (Excursion excursion : reservation.getExcursions()) {
                dtoList.add(dtoConverter.entityToDto(excursion));
            }
        }
        
        reservationDTO.setCustomer( dtoConverter.entityToDto(reservation.getCustomer()) );

        reservationDTO.setExcursions(dtoList);
        
        return reservationDTO;
    }
    
    @Override
    @Transactional 
    public ReservationDTO makeReservation(ReservationDTO dto, String username) {
        Reservation reservation = dtoConverter.dtoToEntity(dto);
        reservation.setTrip(dtoConverter.dtoToEntity(dto.getTrip()));
        
        Customer customer = customerDAO.findByUserName(username);
        reservation.setCustomer(customer);
        
        reservation = reservationDAO.persist(reservation);
        
        return dtoConverter.entityToDto(reservation);
    }       
    
    @Override
    @Transactional 
    public ReservationDTO cancelReservation(Long id) {
        Reservation reservation = reservationDAO.findById(id);
        reservation.setCanceled(true);
        reservationDAO.update(reservation);
        
        ReservationDTO reservationDTO = dtoConverter.entityToDto(reservation);
        return reservationDTO;
    }     
    
    @Override
    @Transactional 
    public void addExcursionToReservation(Long reservationId, Long excursionId) {
        Reservation reservation = reservationDAO.findById(reservationId);
        Excursion excursion = excursionDAO.findById(excursionId);
        
        reservation.getExcursions().add(excursion);
        reservationDAO.persist(reservation);
    }      

    @Override
    public List<ExcursionDTO> listExcursionsForTrip() {
        List<Excursion> entityList = excursionDAO.listAll();
        List<ExcursionDTO> dtoList = new ArrayList<>();
        if (entityList != null) {
            for (Excursion excursion : entityList) {
                dtoList.add(dtoConverter.entityToDto(excursion));
            }
        }
        return dtoList;
    }
    
}
