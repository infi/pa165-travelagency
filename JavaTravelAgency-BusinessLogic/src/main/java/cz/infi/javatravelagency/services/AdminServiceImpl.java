package cz.infi.javatravelagency.services;

import cz.infi.javatravelagency.dao.CustomerDAO;
import cz.infi.javatravelagency.dao.ReservationDAO;
import cz.infi.javatravelagency.dao.TripDAO;
import cz.infi.javatravelagency.dao.ExcursionDAO;
import cz.infi.javatravelagency.entities.Customer;
import cz.infi.javatravelagency.entities.Excursion;
import cz.infi.javatravelagency.entities.Reservation;
import cz.infi.javatravelagency.entities.Trip;
import cz.infi.javatravelagency.dto.CustomerDTO;
import cz.infi.javatravelagency.dto.ExcursionDTO;
import cz.infi.javatravelagency.dto.ReservationDTO;
import cz.infi.javatravelagency.dto.TripDTO;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AdminServiceImpl implements AdminService {
    
    private ReservationDAO reservationDAO;  
    public void setReservationDAO(ReservationDAO reservationDAO) {
        this.reservationDAO = reservationDAO;
    }

    private CustomerDAO customerDAO; 
    public void setCustomerDAO(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }    
    
    private TripDAO tripDAO;  
    public void setTripDAO(TripDAO tripDAO) {
        this.tripDAO = tripDAO;
    }   
    
    private ExcursionDAO excursionDAO;  
    public void setExcursionDAO(ExcursionDAO excursionDAO) {
        this.excursionDAO = excursionDAO;
    }  
    
    private DtoConverterImpl dtoConverter;
    @Required
    public void setDtoConverter(DtoConverterImpl dtoConverter) {
        this.dtoConverter = dtoConverter;
    }     

    @Override
    @Transactional  
    public List<ReservationDTO> listAllReservations() {
        List<ReservationDTO> dtoList = new ArrayList<>();
        List<Reservation> entityList = reservationDAO.listAll();
        for(Reservation entity: entityList) {
            ReservationDTO reservationDTO = dtoConverter.entityToDto(entity);
            
            //Assigned trip
            if (entity.getTrip() != null) { 
                reservationDTO.setTrip(dtoConverter.entityToDto(entity.getTrip()));
            }
            
            //Assigned excursions
            List<ExcursionDTO> excursionsDtoList = new ArrayList<>();
            for(Excursion excursionEntity: entity.getExcursions()) {
                excursionsDtoList.add(dtoConverter.entityToDto(excursionEntity));
            }
            reservationDTO.setExcursions(excursionsDtoList);
                    
            dtoList.add(reservationDTO);
        }
        return dtoList;
    }    
    
    @Override
    @Transactional  
    public List<CustomerDTO> listAllCustomers() {
        List<CustomerDTO> dtoList = new ArrayList<>();
        List<Customer> entityList = customerDAO.listAll();
        for(Customer entity: entityList) {
            dtoList.add(dtoConverter.entityToDto(entity));
        }
        return dtoList;
    }     

    @Override
    @Transactional  
    public List<TripDTO> listAllTrips() {
        List<TripDTO> dtoList = new ArrayList<>();
        List<Trip> entityList = tripDAO.listAll();
        for(Trip entity: entityList) {
            dtoList.add(dtoConverter.entityToDto(entity));
        }
        return dtoList;
    }
 
    @Override
    @Transactional  
    public List<ExcursionDTO> listAllExcursions() {
        List<ExcursionDTO> dtoList = new ArrayList<>();
        List<Excursion> entityList = excursionDAO.listAll();
        for(Excursion entity: entityList) {
            dtoList.add(dtoConverter.entityToDto(entity));
        }
        return dtoList;
    }
    
    @Override
    @Transactional  
    public CustomerDTO getCustomerById(Long id) {
        Customer customer = customerDAO.findById(id);
        if(customer == null)
            return null;
        return dtoConverter.entityToDto(customer);
    } 

    @Override
    public ExcursionDTO getExcursionById(Long id) {
        Excursion excursion = excursionDAO.findById(id);
        if(excursion == null)
            return null;
        return dtoConverter.entityToDto(excursion);
    }

    @Override
    public TripDTO getTripById(Long id) {
        Trip trip = tripDAO.findById(id);
        if(trip == null)
            return null;
        return dtoConverter.entityToDto(trip);
    }
    
    @Override
    @Transactional  
    public void createTrip(TripDTO dto) {
        tripDAO.persist(dtoConverter.dtoToEntity(dto));
    }
    
    @Override
    @Transactional  
    public void createExcursion(ExcursionDTO dto) {
        excursionDAO.persist(dtoConverter.dtoToEntity(dto));
    }   
    
    @Override
    @Transactional  
    public void removeCustomer(Long id) {
        Customer customer = customerDAO.findById(id);
        customerDAO.remove(customer);
    } 
    
    @Override
    @Transactional  
    public void removeTrip(Long id) {
        Trip trip = tripDAO.findById(id); 
        tripDAO.remove(trip);
    }    
    
    @Override
    @Transactional  
    public void removeExcursion(Long id) {
        Excursion excursion = excursionDAO.findById(id);        
        excursionDAO.remove(excursion);
    }

    @Override
    @Transactional  
    public void removeReservation(Long id) {
        Reservation reservation = reservationDAO.findById(id);        
        reservationDAO.remove(reservation);
    }    
    
    @Override
    @Transactional  
    public void updateTrip(TripDTO dto) {
        Trip entity = dtoConverter.dtoToEntity(dto);
        tripDAO.update(entity);
    }    
    
    @Override
    @Transactional  
    public void updateCustomer(CustomerDTO dto) {
        Customer entity = dtoConverter.dtoToEntity(dto);
        customerDAO.update(entity);
    }       
    
    @Override
    @Transactional  
    public void updateExcursion(ExcursionDTO dto) {
        Excursion entity = dtoConverter.dtoToEntity(dto);
        tripDAO.update(entity);
    } 
    
    @Override
    @Transactional  
    public void assignExcursionToTrip(Long excursionId, Long tripId) {
        Trip trip = tripDAO.findById(tripId);
        Excursion excursion = excursionDAO.findById(excursionId);
        List<Excursion> list = trip.getExcursions();
        if(list == null)
            list = new ArrayList<>();
        list.add(excursion);
        trip.setExcursions(list);
        tripDAO.update(trip);
    }      
    
    @Override
    @Transactional 
    public CustomerDTO createCustomer(CustomerDTO dto) {
        Customer customer = customerDAO.persist(dtoConverter.dtoToEntity(dto));
        return dtoConverter.entityToDto(customer);
    }    

}
