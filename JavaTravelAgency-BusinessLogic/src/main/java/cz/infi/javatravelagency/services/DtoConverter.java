package cz.infi.javatravelagency.services;

import cz.infi.javatravelagency.entities.Customer;
import cz.infi.javatravelagency.entities.Excursion;
import cz.infi.javatravelagency.entities.Reservation;
import cz.infi.javatravelagency.entities.Trip;
import cz.infi.javatravelagency.dto.CustomerDTO;
import cz.infi.javatravelagency.dto.ExcursionDTO;
import cz.infi.javatravelagency.dto.ReservationDTO;
import cz.infi.javatravelagency.dto.TripDTO;

/**
 *
 * @author infi
 */
public interface DtoConverter {

    Trip dtoToEntity(TripDTO dto);

    Reservation dtoToEntity(ReservationDTO dto);

    Excursion dtoToEntity(ExcursionDTO dto);

    Customer dtoToEntity(CustomerDTO dto);

    TripDTO entityToDto(Trip entity);

    ReservationDTO entityToDto(Reservation entity);

    ExcursionDTO entityToDto(Excursion entity);

    CustomerDTO entityToDto(Customer entity);
    
}
