package cz.infi.javatravelagency.services;

import cz.infi.javatravelagency.dao.CustomerDAO;
import cz.infi.javatravelagency.dto.CustomerDTO;
import cz.infi.javatravelagency.entities.Customer;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Michael Musil
 */
@Service
public class PublicServiceImpl implements PublicService {
    
    private CustomerDAO customerDAO; 
    public void setCustomerDAO(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }  

    private DtoConverterImpl dtoConverter;
    @Required  
    public void setDtoConverter(DtoConverterImpl dtoConverter) {
        this.dtoConverter = dtoConverter;
    }        
    
    @Override
    @Transactional 
    public CustomerDTO createCustomer(CustomerDTO dto) {
        Customer customer = customerDAO.persist(dtoConverter.dtoToEntity(dto));
        return dtoConverter.entityToDto(customer);
    }      
}
