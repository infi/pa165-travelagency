package cz.infi.javatravelagency.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ondrej Fujtik
 */

/**
 * Class,serving as a basic Data Access Object class providing CRUD operations
 * and as a Excursion DAO class
 */

@Repository
public abstract class BaseDAOImpl implements GenericDAOI {
    
    protected Class entityClass;
    
    @PersistenceContext
    protected EntityManager entityManager;
    
    public BaseDAOImpl(){
        
    }    

    public void setEntityClass(Class entityClass) {
        this.entityClass = entityClass;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    
    public <ENTITY> ENTITY persist(ENTITY entity){
       entityManager.persist(entity);
       return entity;
     }
    
    public <ENTITY> ENTITY remove (ENTITY entity){
        entityManager.remove(entity);
        return entity;
    }
    
    public <ENTITY, K> ENTITY findById(K id){
        return (ENTITY)entityManager.find(entityClass, id);
    }
    
    public <ENTITY> ENTITY update(ENTITY entity){
       ENTITY r = entityManager.merge(entity);
       return r;
    }
}
