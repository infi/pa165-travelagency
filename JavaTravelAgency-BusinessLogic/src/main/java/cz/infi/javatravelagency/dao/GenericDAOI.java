package cz.infi.javatravelagency.dao;

/**
 *
 * @author Ondrej Fujtik
 */

/**
 * Basic generic Data Access Object Interface
 * 
 */
public interface GenericDAOI {
    /**
     * Stores object in the system
     * @param <ENTITY> Type of object to store
     * @param entity Object to store
     * @return Stored object
     */
    public <ENTITY> ENTITY persist(ENTITY entity);
    /**
     * Removes object from the system
     * @param <ENTITY> Type of object to remove
     * @param entity Object to remove
     * @return Removed object
     */
    public <ENTITY> ENTITY remove(ENTITY entity);
   
    /**
     * Finds an object by it's key value
     * @param <ENTITY> Type of the object to find
     * @param <K> Type of key value of the object
     * @param Id Key value of the object
     * @return {@code null} if object was not found with this key value.
     * Else {@code ENTITY}
     */
    public <ENTITY,K> ENTITY findById(K Id);
    /**
     * Updates the object
     * @param <ENTITY> Type of the object to update
     * @param entity Object to be updated
     * @return Updated object
     */
    public <ENTITY> ENTITY update(ENTITY entity);
}
