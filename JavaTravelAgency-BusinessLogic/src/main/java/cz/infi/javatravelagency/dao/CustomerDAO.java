
package cz.infi.javatravelagency.dao;

import cz.infi.javatravelagency.entities.Customer;
import java.util.List;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;

/**
 * Customer DAO
 * @author Michael Musil
 */
@Repository
public class CustomerDAO extends BaseDAOImpl{

    public CustomerDAO(){
        super();
        this.entityClass = Customer.class;
    }   
    public List<Customer> listAll(){
        Query q = entityManager.createQuery("SELECT c FROM Customer c");
        return q.getResultList();
    }
    
    public Customer findByUserName(String username){
        Query q = entityManager.createQuery("SELECT c FROM Customer c WHERE c.username = ?1");
        q.setParameter(1, username);

        List<Customer> results = q.getResultList();

        if (!q.getResultList().isEmpty()) return results.get(0);
        else return null;
    }
}
