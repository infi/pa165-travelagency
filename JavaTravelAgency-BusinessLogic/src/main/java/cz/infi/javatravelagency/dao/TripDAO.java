package cz.infi.javatravelagency.dao;

import cz.infi.javatravelagency.entities.Excursion;
import cz.infi.javatravelagency.entities.Trip;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Scotty
 */
@Repository
public class TripDAO extends BaseDAOImpl{
    public TripDAO(){
        super();
        this.entityClass = Trip.class;
    } 
  
    
    public List<Excursion> listAllExcursions(){
        Query q=entityManager.createQuery("SELECT e.excursions FROM Trip e");
        return q.getResultList();
    }
    
    public List<Trip> listAll(){
        Query q=entityManager.createQuery("SELECT e FROM Trip e");
        return q.getResultList();
    }
    
    public List<Trip> listAvailableTrips(){
        Query q=entityManager.createQuery("SELECT e FROM Trip e WHERE availableTrips > 0");
        return q.getResultList();
    }
    
}
