
package cz.infi.javatravelagency.dao;

import cz.infi.javatravelagency.entities.Excursion;
import java.util.List;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;

@Repository
public class ExcursionDAO extends BaseDAOImpl{
    public ExcursionDAO(){
        super();
        this.entityClass = Excursion.class;
    }   
    public List<Excursion> listAll(){
        Query q = entityManager.createQuery("SELECT c FROM Excursion c");
        return q.getResultList();
    }
}
