package cz.infi.javatravelagency.dao;

import cz.infi.javatravelagency.entities.Reservation;
import cz.infi.javatravelagency.entities.Trip;
import cz.infi.javatravelagency.entities.Excursion;
import java.util.List;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;

@Repository
public class ReservationDAO extends BaseDAOImpl{
    public ReservationDAO(){
        super();
        this.entityClass = Reservation.class;
    }   
    public List<Reservation> listAll(){
        Query q=entityManager.createQuery("SELECT c FROM Reservation c");
        return q.getResultList();
    }
    public List<Reservation> listByTrip(Trip trip){
        Query q=entityManager.createQuery("SELECT c FROM Reservation c WHERE c.trip = ?1")
                             .setParameter(1, trip);
        return q.getResultList();
    }
    
    public List<Reservation> listByExcursion(Excursion excursion){
        Query q=entityManager.createQuery("SELECT c FROM Reservation c"
                                        + " JOIN c.excursions e "
                                        + " WHERE  e.id = ?1")
                             .setParameter(1, excursion.getId());
                
        return q.getResultList();
    }
}
