
package cz.infi.javatravelagency;

import org.glassfish.jersey.server.ResourceConfig;

/**
 *
 * @author Libor
 */
public class MyApplication extends ResourceConfig {

    /*Register JAX-RS application components.*/
    public MyApplication () {
        register(TripResource.class);
        register(ExcursionResource.class);
    }
}