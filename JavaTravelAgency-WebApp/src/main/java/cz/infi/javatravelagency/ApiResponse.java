package cz.infi.javatravelagency;

/**
 *
 * @author Libor
 */
public class ApiResponse {
    
    private String code;
    private String message;
    
    public String getCode() {
        return code;
    }

    public void setCode(String id) {
        this.code = id;
    }
    
    public String getMessage() {
        return message;
    }

    public void setMessage(String id) {
        this.message = id;
    }
    
    public ApiResponse()
    {
    }
    
    public ApiResponse(int code, String message)
    {
        this.code = Integer.toString(code);
        this.message = message;
    }
    
    public ApiResponse(String code, String message)
    {
        this.code = code;
        this.message = message;
    }
}
