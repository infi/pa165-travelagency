
package cz.infi.javatravelagency;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import org.apache.taglibs.standard.functions.Functions;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author scotty
 */
public abstract class BaseActionBean implements ActionBean {
    private ActionBeanContext context;

    public String username = "";
    
    public BaseActionBean() {
        loadUser();
    }
    
    @Override
    public void setContext(ActionBeanContext context) {
        this.context = context;
    }

    @Override
    public ActionBeanContext getContext() {
        return context;
    }

    public String getUsername() {
        return username;
    }

    
    public static String escapeHTML(String s) {
        return Functions.escapeXml(s);
    }
    
    
    public final void loadUser() {
      Authentication auth = SecurityContextHolder.getContext().getAuthentication();
      this.username = auth.getName(); 
    }    
}
