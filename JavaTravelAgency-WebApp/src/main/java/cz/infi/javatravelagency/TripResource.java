package cz.infi.javatravelagency;

import cz.infi.javatravelagency.dto.TripDTO;
import cz.infi.javatravelagency.exceptions.MissingAttributesException;
import cz.infi.javatravelagency.exceptions.NotFoundException;
import cz.infi.javatravelagency.services.AdminService;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Libor
 */
@Component
@Path("trip")
public class TripResource {
 
    @Context
    private UriInfo context;
    
    @Autowired
    protected AdminService adminService;
  
    public TripResource() {
    }
 
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<TripDTO> getText() {
        return adminService.listAllTrips();
    }
    
    @GET
    @Path("{id}")
    public TripDTO getTrip(@PathParam("id") Long id) {
        TripDTO trip = adminService.getTripById(id);
        if(trip == null)
            throw new NotFoundException("Trip not found");
        return trip;
    }
    
    @GET
    @Path("json/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public TripDTO getJson(@PathParam("id") Long id) {
        TripDTO trip = adminService.getTripById(id);
        if(trip == null)
            throw new NotFoundException("Trip not found");
        return trip;
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ApiResponse postJson(TripDTO trip) {
        if(trip.getDestination() == null || trip.getAvailableTrips() == null || trip.getDateFrom() == null || trip.getDateTo() == null)
        {
            throw new MissingAttributesException("There are missing attributes in request");
        }
        adminService.createTrip(trip);
        ApiResponse response = new ApiResponse("200", "Trip was created");
        return response;
    }
    
    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ApiResponse putJson(@PathParam("id") Long id, TripDTO trip) {
        if(trip.getDestination() == null || trip.getAvailableTrips() == null || trip.getDateFrom() == null || trip.getDateTo() == null)
        {
            throw new MissingAttributesException("There are missing attributes in request");
        }
        adminService.updateTrip(trip);
        ApiResponse response = new ApiResponse("200", "Trip was updated");
        return response;
    } 
    
    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String getCount() {
        return String.valueOf(adminService.listAllTrips().size());
    }

}
