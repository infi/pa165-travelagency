package cz.infi.javatravelagency.exceptions;

import cz.infi.javatravelagency.ApiResponse;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 *
 * @author Libor
 */
public class MissingAttributesException extends WebApplicationException {
        public MissingAttributesException(String message) {
        super(Response.status(Response.Status.BAD_REQUEST).
                entity(new ApiResponse(Response.Status.BAD_REQUEST.getStatusCode(), message))
                .build());
    }
}
