package cz.infi.javatravelagency.exceptions;

import cz.infi.javatravelagency.ApiResponse;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 *
 * @author Libor
 */
public class NotFoundException extends WebApplicationException {

    public NotFoundException(String message) {
        super(Response.status(Response.Status.NOT_FOUND).
                entity(new ApiResponse(Response.Status.NOT_FOUND.getStatusCode(), message))
                .build());
    }

}
