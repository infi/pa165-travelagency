/**
 *
 * @author scotty
 */

package cz.infi.javatravelagency;

import static cz.infi.javatravelagency.BaseActionBean.escapeHTML;
import net.sourceforge.stripes.action.*;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;

import java.util.List;

import cz.infi.javatravelagency.services.*;
import cz.infi.javatravelagency.dto.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sourceforge.stripes.validation.LocalizableError;

@UrlBinding("/admin/{$event}/{$id}")
public class AdminActionBean extends BaseActionBean implements ValidationErrorHandler {
    
    @SpringBean //Spring can inject even to private and protected fields
    protected AdminService adminService;

    @SpringBean
    protected GuestService guestService;
        
    //Data for customers
    private List<CustomerDTO> customers;
    @ValidateNestedProperties(value = {
            @Validate(on = {"addCustomer"}, field = "name", required = true),
            @Validate(on = {"addCustomer"}, field = "surname", required = true),
            @Validate(on = {"addCustomer"}, field = "username", required = true),
            @Validate(on = {"addCustomer"}, field = "password", required = true),
            @Validate(on = {"addCustomer"}, field = "contacts", required = true),
            @Validate(on = {"addCustomer"}, field = "address", required = true),
            @Validate(on = {"addCustomer"}, field = "phone", required = true, maxlength=13),
    })
    private CustomerDTO customer;
    
    //Data for Excursion
    private List<ExcursionDTO> excursions;
    @ValidateNestedProperties(value = {
            @Validate(on = {"addExcursion"}, field = "excursionDate", required = true),
            @Validate(on = {"addExcursion"}, field = "duration", required = true, maxlength=10),
            @Validate(on = {"addExcursion"}, field = "description", required = true),
            @Validate(on = {"addExcursion"}, field = "destination", required = true, maxlength=200),
            @Validate(on = {"addExcursion"}, field = "price", required = true)
    })
    private ExcursionDTO excursion;
    
    //Data for Reservation
    private List<ReservationDTO> reservations;
    private ReservationDTO reservation;
    private List<ExcursionDTO> availableExcursions;
    
    //Data for Trip
    private List<TripDTO> trips;
    @ValidateNestedProperties(value = {
            @Validate(on = {"addTrip"}, field = "dateFrom", 
                                        required = true, 
                                        expression="trip.dateFrom > today "
                                              + "&& trip.dateFrom < trip.dateTo"),
            @Validate(on = {"addTrip"}, field = "dateTo", 
                                        required = true,
                                        expression="trip.dateTo > today "
                                              + "&& trip.dateFrom < trip.dateTo"),
            @Validate(on = {"addTrip"}, field = "destination", required = true, maxlength=200),
            @Validate(on = {"addTrip"}, field = "availableTrips", required = true),
    })
    private TripDTO trip;
    
    //Zobrazeni zakladni stranky customers
    @DefaultHandler
    public Resolution index() {
        trips = adminService.listAllTrips();
        return new ForwardResolution("/Admin/index.jsp"); 
    }
    
    // ---- Customers ----
    public Resolution customers() {
        customers = adminService.listAllCustomers();
        return new ForwardResolution("/Customer/customer.jsp"); 
    }
    
    public Resolution addCustomer() {
        adminService.createCustomer(convertPasswordFromCustomer(getCustomer()));
        getContext().getMessages().add(new LocalizableMessage("customer.add.message",escapeHTML(customer.getName()),escapeHTML(customer.getSurname())));
        return new RedirectResolution(this.getClass(), "customers");
    }

    @Before(stages = LifecycleStage.BindingAndValidation, on = {"editCustomer", "saveCustomer"})
    public void loadCustomerFromDatabase() {
        String ids = getContext().getRequest().getParameter("customer.id");
        if (ids == null) return;
        customer = adminService.getCustomerById(Long.parseLong(ids));
    }
    
    public Resolution editCustomer() {
        return new ForwardResolution("/Customer/edit.jsp");
    } 

    public Resolution saveCustomer() {        
        adminService.updateCustomer(convertPasswordFromCustomer(customer));
        return new RedirectResolution(this.getClass(), "customers");
    }
    
    public Resolution deleteCustomer() {
        adminService.removeCustomer(customer.getId());
        return new RedirectResolution(this.getClass(), "customers");
    }
    
    // ---- Excursions ----
    @Before(stages = LifecycleStage.BindingAndValidation, on = {"excursions","editExcursion"})
    public void loadAllTripsFromDatabase() {
        trips = adminService.listAllTrips();
    }
    
    public Resolution excursions() {
        excursions = adminService.listAllExcursions();
        return new ForwardResolution("/Excursion/excursion.jsp"); 
    }
    
    public Resolution addExcursion() {
        
        String tripId = getContext().getRequest().getParameter("trip.id");
        adminService.createExcursion(excursion);
        excursions = adminService.listAllExcursions();
        for (ExcursionDTO exc : excursions) {
            if(excursion.getDescription().equals(exc.getDescription()))
                excursion = exc;
        }
        if ( trip != null ) {
            adminService.assignExcursionToTrip(excursion.getId(), trip.getId());
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        getContext().getMessages().add(new LocalizableMessage("excursion.add.message",escapeHTML(dateFormat.format(excursion.getExcursionDate())),escapeHTML(excursion.getDestination())));
        return new RedirectResolution(this.getClass(), "excursions");
    }

    @Before(stages = LifecycleStage.BindingAndValidation, on = {"editExcursion", "saveExcursion"})
    public void loadExcursionFromDatabase() {
        String ids = getContext().getRequest().getParameter("excursion.id");
        if (ids == null) return;
        excursion = adminService.getExcursionById(Long.parseLong(ids));
    }
    
    public Resolution editExcursion() {
        return new ForwardResolution("/Excursion/edit.jsp");
    } 

    public Resolution saveExcursion() {
        adminService.updateExcursion(excursion);
        return new RedirectResolution(this.getClass(), "excursions");
    }
    
    public Resolution deleteExcursion() {
        try {
            adminService.removeExcursion(excursion.getId());
        } catch (Exception e) {
            getContext().getMessages().add(new LocalizableMessage("excursion.remove.error"));
        }
        return new RedirectResolution(this.getClass(), "excursions");
    }
     
    // ---- Trips ----
    public Resolution trips() {
        trips = adminService.listAllTrips();
        return new ForwardResolution("/Trip/list.jsp"); 
    }
    
    
    public Resolution addTrip() {
        adminService.createTrip(trip);
        getContext().getMessages().add(new LocalizableMessage("trip.add.message",escapeHTML(trip.getDestination())));
        return new RedirectResolution(this.getClass(), "trips");
    }
    
    @Before(stages = LifecycleStage.BindingAndValidation, on = {"editTrip", "saveTrip","addExcursion"})
    public void loadTripFromDatabase() {
        String ids = getContext().getRequest().getParameter("trip.id");
        if (ids == null) return;
        trip = adminService.getTripById(Long.parseLong(ids));
    }
    
    public Resolution editTrip() {
        return new ForwardResolution("/Trip/edit.jsp");
    } 

    public Resolution saveTrip() {
        adminService.updateTrip(trip);
        return new RedirectResolution(this.getClass(), "trips");
    }
    
    public Resolution deleteTrip() {
        try {
            adminService.removeTrip(trip.getId());
        } catch (Exception e) {
            getContext().getMessages().add(new LocalizableError("trip.remove.error"));
        }
        return new RedirectResolution(this.getClass(), "trips");
    }
    
    
    // ---- Reservations ----
    public Resolution reservations() {
        reservations = adminService.listAllReservations();
        return new ForwardResolution("/Reservation/reservation.jsp"); 
    }
    
    @Before(stages = LifecycleStage.BindingAndValidation, on = {"reservationDetail","reservationAddExcursion","reservationCancel"})
    public void loadReservationFromDatabase() {
        String ids = getContext().getRequest().getParameter("reservation.id");
        if (ids == null) return;
        setReservation(guestService.reservationDetail(Long.parseLong(ids)));
    }
    
    public Resolution reservationDetail() {
        setAvailableExcursions(guestService.listExcursionsForTrip());
        return new ForwardResolution("/Reservation/detail.jsp"); 
    }
    
    public Resolution deleteReservation() {
        try {
            adminService.removeReservation(reservation.getId());
        } catch (Exception e) {
            getContext().getMessages().add(new LocalizableError("reservation.remove.error"));
        }
        return new RedirectResolution(this.getClass(), "reservations");
    }
    
    public Resolution reservationAddExcursion() {
        String excursionId = getContext().getRequest().getParameter("excursion.id");
        guestService.addExcursionToReservation(getReservation().getId(), Long.parseLong(excursionId));
        return new RedirectResolution(this.getClass(), "reservationDetail").addParameter("reservation.id", getReservation().getId());
    }
    
    private CustomerDTO convertPasswordFromCustomer(CustomerDTO customer){
        String password = customer.getPassword();
        try {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(password.getBytes());
            byte byteData[] = md.digest();
            
            //convert the byte to hex format method 2
            StringBuffer hexString = new StringBuffer();
            for (int i=0;i<byteData.length;i++) {
                    String hex=Integer.toHexString(0xff & byteData[i]);
                    if(hex.length()==1) hexString.append('0');
                    hexString.append(hex);
            }
            
            customer.setPassword(hexString.toString());
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(RegistrationActionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
                    
        return customer;
    }
    
    // ---- getters and setters ----
    public List<CustomerDTO> getCustomers() {
        return customers;
    }
    
    public List<ExcursionDTO> getExcursions() {
        return excursions;
    }
    
    public List<ReservationDTO> getReservations() {
        return reservations;
    }

    public List<TripDTO> getTrips() {
        return trips;
    }

    public void setTrip(TripDTO trip) {
        this.trip = trip;
    }

    public TripDTO getTrip() {
        return trip;
    }
    
    public void setExcursion(ExcursionDTO excursion) {
        this.excursion = excursion;
    }

    public ExcursionDTO getExcursion() {
        return excursion;
    }
    
    public Date getToday() {
        return new Date();
    }
    
    public List<ExcursionDTO> getAvailableExcursions() {
        return availableExcursions;
    }

    public void setAvailableExcursions(List<ExcursionDTO> availableExcursions) {
        this.availableExcursions = availableExcursions;
    }    
    
    @Override
    public Resolution handleValidationErrors(ValidationErrors errors) throws Exception {
        return null;
    }

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }
 
    public Resolution getSourcePageResolution()
    {
        return new ForwardResolution("/Reservation/reservation.jsp"); 
    }

    /**
     * @return the reservation
     */
    public ReservationDTO getReservation() {
        return reservation;
    }

    /**
     * @param reservation the reservation to set
     */
    public void setReservation(ReservationDTO reservation) {
        this.reservation = reservation;
    }
    
}
