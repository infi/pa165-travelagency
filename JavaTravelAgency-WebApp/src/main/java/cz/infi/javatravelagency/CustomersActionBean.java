
package cz.infi.javatravelagency;

import net.sourceforge.stripes.action.*;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import java.util.List;
import cz.infi.javatravelagency.services.*;
import cz.infi.javatravelagency.entities.*;

/**
 *
 * @author scotty
 */
@UrlBinding("/customers/{$event}/{customer.id}")
public class CustomersActionBean extends BaseActionBean implements ValidationErrorHandler {
    
    @SpringBean //Spring can inject even to private and protected fields
    protected AdminService adminService;

    //--- part for showing a list of customers ----
    private List<Customer> customers;
    
    //Zobrazeni zakladni stranky customers
    @DefaultHandler
    public Resolution list() {
        return new ForwardResolution("/Customer/create.jsp"); 
    }
    
    
    public List<Customer> getCustomers() {
        return customers;
    }
    
    
    @Override
    public Resolution handleValidationErrors(ValidationErrors errors) throws Exception {
        //fill up the data for the table if validation errors occured
        return null;
    }
    
    private Customer customer;
    
}
