package cz.infi.javatravelagency;

import cz.infi.javatravelagency.dto.ExcursionDTO;
import cz.infi.javatravelagency.dto.ReservationDTO;
import net.sourceforge.stripes.action.*;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.integration.spring.SpringBean;

import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import java.util.List;

import cz.infi.javatravelagency.services.*;

/**
 *
 * @author infi
 */
@UrlBinding("/guest/reservation/{$event}/{reservation.id}")
public class GuestReservationActionBean extends BaseActionBean implements ValidationErrorHandler {
    
    @SpringBean //Spring can inject even to private and protected fields
    protected GuestService guestService;

    private List<ReservationDTO> reservations;
    private ReservationDTO reservation;
    
    private List<ExcursionDTO> availableExcursions;
    private ExcursionDTO excursion; 
    
    // -- actions --
    @DefaultHandler
    public Resolution list() {         
        return new ForwardResolution("/Guest/reservation/list.jsp"); 
    }
    public Resolution detail() {
        setAvailableExcursions(guestService.listExcursionsForTrip());
        return new ForwardResolution("/Guest/reservation/detail.jsp"); 
    }
    
    public Resolution addExcursion() {
        String excursionId = getContext().getRequest().getParameter("excursion.id");
        guestService.addExcursionToReservation(reservation.getId(), Long.parseLong(excursionId));
        return new RedirectResolution(this.getClass(), "detail").addParameter("reservation.id", reservation.getId());
    }
    
    public Resolution cancel() {
        guestService.cancelReservation(reservation.getId());
        return new RedirectResolution(this.getClass(), "detail").addParameter("reservation.id", reservation.getId());
    }    
    
    // -- others --
    @Before(stages = LifecycleStage.BindingAndValidation, on = {"detail","addExcursion","cancel"})
    public void loadReservationFromDatabase() {
        String ids = getContext().getRequest().getParameter("reservation.id");
        if (ids == null) return;
        reservation = guestService.reservationDetail(Long.parseLong(ids));
    }
     
    @Override
    public Resolution handleValidationErrors(ValidationErrors errors) throws Exception {
        return null;
    }
    
    public List<ReservationDTO> getReservations() {
        return reservations;
    }

    public void setReservation(ReservationDTO reservation) {
        this.reservation = reservation;
    }

    public ReservationDTO getReservation() {
        return reservation;
    }

    public List<ExcursionDTO> getAvailableExcursions() {
        return availableExcursions;
    }

    public void setAvailableExcursions(List<ExcursionDTO> availableExcursions) {
        this.availableExcursions = availableExcursions;
    }

    public ExcursionDTO getExcursion() {
        return excursion;
    }

    public void setExcursion(ExcursionDTO excursion) {
        this.excursion = excursion;
    }
}
