package cz.infi.javatravelagency;

import java.io.IOException;
import javax.servlet.http.*;
import java.util.Set;
import javax.servlet.ServletException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

/**
 *
 * @author Skoták Miloš
 */


public class CustomAuthenticationHandler extends SimpleUrlAuthenticationSuccessHandler {
 
 @Override
 public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
      String userTargetUrl = "/guest/trip";
      String adminTargetUrl = "/admin/index";
      Set<String> roles = AuthorityUtils.authorityListToSet(authentication.getAuthorities());
      if (roles.contains("ROLE_ADMIN")) {
         getRedirectStrategy().sendRedirect(request, response, adminTargetUrl);
      } else if (roles.contains("ROLE_USER")) {
         getRedirectStrategy().sendRedirect(request, response, userTargetUrl);
      } else {
         super.onAuthenticationSuccess(request, response, authentication);
      }
   }
}
