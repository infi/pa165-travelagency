package cz.infi.javatravelagency;

import cz.infi.javatravelagency.dto.ExcursionDTO;
import cz.infi.javatravelagency.exceptions.MissingAttributesException;
import cz.infi.javatravelagency.exceptions.NotFoundException;
import cz.infi.javatravelagency.services.AdminService;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Libor
 */
@Component
@Path("excursion")
public class ExcursionResource {
 
    @Context
    private UriInfo context;
    
    @Autowired
    protected AdminService adminService;
  
    public ExcursionResource() {
    }
 
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<ExcursionDTO> getText() {
        return adminService.listAllExcursions();
    }
    
    @GET
    @Path("{id}")
    public ExcursionDTO getReservation(@PathParam("id") Long id) {
        ExcursionDTO excursion = adminService.getExcursionById(id);
        if(excursion == null)
            throw new NotFoundException("Excursion not found");
        return excursion;
    }
    
    @GET
    @Path("json/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ExcursionDTO getJson(@PathParam("id") Long id) {
        ExcursionDTO excursion = adminService.getExcursionById(id);
        if(excursion == null)
            throw new NotFoundException("Excursion not found");
        return excursion;
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ApiResponse postJson(ExcursionDTO excursion) {
        if(excursion.getDestination() == null || excursion.getDescription() == null || excursion.getDuration() == 0 || excursion.getExcursionDate() == null)
        {
            throw new MissingAttributesException("There are missing attributes in request");
        }
        adminService.createExcursion(excursion);
        ApiResponse response = new ApiResponse("200", "Excursion was created");
        return response;
    }
 
    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String getCount() {
        return String.valueOf(adminService.listAllExcursions().size());
    }

}
