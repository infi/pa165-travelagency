package cz.infi.javatravelagency;

import cz.infi.javatravelagency.dto.ReservationDTO;
import cz.infi.javatravelagency.dto.TripDTO;
import net.sourceforge.stripes.action.*;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.integration.spring.SpringBean;

import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import java.util.List;

import cz.infi.javatravelagency.services.*;
import java.util.Date;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;


@UrlBinding("/guest/trip/{$event}/{trip.id}")
public class GuestTripActionBean extends BaseActionBean implements ValidationErrorHandler {
    
    @SpringBean //Spring can inject even to private and protected fields
    protected GuestService guestService;

    private List<TripDTO> trips;
    private TripDTO trip; 
    
    @ValidateNestedProperties(value = {
        @Validate(on={"makeReservation"}, field="numberOfAttendants", required=true)
            
    })
    private ReservationDTO reservation;
    
    // -- actions --

    @DefaultHandler
    public Resolution list() {         
        trips = guestService.listAvailableTrips();
        return new ForwardResolution("/Guest/trip/listAvailableTrips.jsp"); 
    }
    
    public Resolution detail() {
        return new ForwardResolution("/Guest/trip/detail.jsp"); 
    }    
    
    
    public Resolution makeReservation() {  
        reservation.setDate(new Date());
        reservation.setTrip(trip);
        
        if (!"".equals(this.username)) {
            reservation = guestService.makeReservation(reservation,this.username);
        }
        
        
        
        
        return new RedirectResolution(GuestReservationActionBean.class, "detail").addParameter("reservation.id", reservation.getId());
    }
    
    
    // -- others --
   
    @Before(stages = LifecycleStage.BindingAndValidation, on = {"detail","makeReservation"})
    public void loadTripFromDatabase() {
        String ids = getContext().getRequest().getParameter("trip.id");
        if (ids == null) return;
        trip = guestService.tripDetail(Long.parseLong(ids));
    } 

    @Override
    public Resolution handleValidationErrors(ValidationErrors errors) throws Exception {
        return null;
    }
    
    public List<TripDTO> getTrips() {
        return trips;
    }

    public void setTrip(TripDTO trip) {
        this.trip = trip;
    }

    public TripDTO getTrip() {
        return trip;
    }

    /**
     * @return the reservation
     */
    public ReservationDTO getReservation() {
        return reservation;
    }

    /**
     * @param reservation the reservation to set
     */
    public void setReservation(ReservationDTO reservation) {
        this.reservation = reservation;
    }
    
}
