/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.infi.javatravelagency;

import cz.infi.javatravelagency.dto.CustomerDTO;
import net.sourceforge.stripes.action.*;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import cz.infi.javatravelagency.services.*;
import java.util.List;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Skoták Miloš
 */

@UrlBinding("/registration/{$event}/{customer.id}")
public class RegistrationActionBean extends BaseActionBean implements ValidationErrorHandler {
    
    @SpringBean //Spring can inject even to private and protected fields
    protected AdminService adminService;

    @SpringBean //Spring can inject even to private and protected fields
    protected PublicService publicService;
    
    //Zobrazeni zakladni stranky
    @DefaultHandler
    public Resolution list() {
        return new ForwardResolution("/Registration/create.jsp"); 
    }
    
    //Data for customers
    private List<CustomerDTO> customers;
    @ValidateNestedProperties(value = {
            @Validate(on = {"addCustomer"}, field = "name", required = true),
            @Validate(on = {"addCustomer"}, field = "surname", required = true),
            @Validate(on = {"addCustomer"}, field = "username", required = true),
            @Validate(on = {"addCustomer"}, field = "password", required = true),
            @Validate(on = {"addCustomer"}, field = "contacts", required = true),
            @Validate(on = {"addCustomer"}, field = "address", required = true),
            @Validate(on = {"addCustomer"}, field = "phone", required = true, maxlength=13),
    })
    private CustomerDTO customer;
    
    public Resolution addCustomer() {
        publicService.createCustomer(convertPasswordFromCustomer(getCustomer()));
        getContext().getMessages().add(new LocalizableMessage("customer.add.message",escapeHTML(customer.getName()),escapeHTML(customer.getSurname())));
        return new RedirectResolution(this.getClass(), "customers");
    }
    
    @Before(stages = LifecycleStage.BindingAndValidation, on = {"saveCustomer"})
    public void loadCustomerFromDatabase() {
        String ids = getContext().getRequest().getParameter("customer.id");
        if (ids == null) return;
        customer = adminService.getCustomerById(Long.parseLong(ids));
    }
    
    private CustomerDTO convertPasswordFromCustomer(CustomerDTO customer){
        String password = customer.getPassword();
        try {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(password.getBytes());
            byte byteData[] = md.digest();
            
            //convert the byte to hex format method 2
            StringBuffer hexString = new StringBuffer();
            for (int i=0;i<byteData.length;i++) {
                    String hex=Integer.toHexString(0xff & byteData[i]);
                    if(hex.length()==1) hexString.append('0');
                    hexString.append(hex);
            }
            
            customer.setPassword(hexString.toString());
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(RegistrationActionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
                    
        return customer;
    }
       
    
    @Override
    public Resolution handleValidationErrors(ValidationErrors errors) throws Exception {
        //fill up the data for the table if validation errors occured
        return null;
    }

    
    // ---- getters and setters ----
    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }
    
    
}
