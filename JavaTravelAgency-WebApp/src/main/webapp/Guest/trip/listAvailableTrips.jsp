<%-- 
    Document   : listAvailableTrips
    Created on : 21-Nov-2013, 00:27:35
    Author     : infi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<s:errors/>

<s:layout-render name="/layout.jsp" titlekey="admin.excursion.title">
    <s:layout-component name="head">
    
    </s:layout-component>
    
    <s:layout-component name="menu">
        <li><s:link event="index" beanclass="cz.infi.javatravelagency.GuestTripActionBean"><f:message key="admin.index.menu.about"/></s:link></li>                                
        <li><a href="<c:url value="/j_spring_security_logout" />"><f:message key="admin.index.menu.logout"/></a></li> 
    </s:layout-component>

    
    <s:layout-component name="body">
        <h2><f:message key="guest.trip.availableTrips"/></h2>
        <table class="table">
            <thead>
                <tr>
                    <th>
                        Id
                    </th>
                    <th>
                        <f:message key="guest.trip.destination"/>
                    </th>
                    <th>
                        <f:message key="guest.trip.dateFrom"/>
                    </th>
                    <th>
                        <f:message key="guest.trip.dateTo"/>
                    </th>
                    <th>
                        <f:message key="guest.trip.availableTrips"/>
                    </th>
                    <th>
                        
                    </th>
                </tr>
            </thead>
            <tbody>
             <c:forEach items="${actionBean.trips}" var="trip">
                 <tr>
                     <td>${trip.id}</td>
                     <td><c:out value="${trip.destination}"/></td>
                     <td><c:out value="${trip.dateFrom}"/></td>
                     <td><c:out value="${trip.dateTo}"/></td>
                     <td><c:out value="${trip.availableTrips}"/></td>
                     <td>
                      <%--<s:link beanclass="cz.muni.fi.pa165.books.BooksActionBean" event="edit"><s:param name="book.id" value="${book.id}"/>edit</s:link>--%>
                     </td>
                     <td>
                         <s:form beanclass="cz.infi.javatravelagency.GuestTripActionBean">
                             <s:hidden name="trip.id" value="${trip.id}"/>
                             <s:submit name="detail"><f:message key="guest.makeReservation"/></s:submit>
                         </s:form>
                     </td>
                 </tr>
             </c:forEach>
            </tbody>
        </table>            
    </s:layout-component>
        
     
</s:layout-render>