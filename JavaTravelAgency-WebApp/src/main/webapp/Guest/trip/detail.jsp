<%-- 
    Document   : listAvailableTrips
    Created on : 21-Nov-2013, 00:27:35
    Author     : infi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>

<s:layout-render name="/layout.jsp" titlekey="admin.excursion.title">
    <s:layout-component name="head">
      
    </s:layout-component>
    
    <s:layout-component name="menu">
        <li><s:link event="index" beanclass="cz.infi.javatravelagency.GuestTripActionBean"><f:message key="admin.index.menu.about"/></s:link></li>                                
        <li><a href="<c:url value="/j_spring_security_logout" />"><f:message key="admin.index.menu.logout"/></a></li> 
    </s:layout-component>
        
    <s:layout-component name="body">
        <h2><f:message key="guest.makeReservation"/></h2>
        <s:errors/>
        <br style="clear: both;"/>
        <s:form beanclass="cz.infi.javatravelagency.GuestTripActionBean">
            <table class="table">
                <tbody>
                    <tr>
                        <td><f:message key="guest.trip.destination"/></td>
                        <td><c:out value="${actionBean.trip.destination}"/></td>
                    </tr>
                    <tr>
                        <td><f:message key="guest.trip.dateFrom"/></td>
                        <td><c:out value="${actionBean.trip.dateFrom}"/></td>
                    </tr>
                    <tr>
                        <td><f:message key="guest.trip.dateTo"/></td>
                        <td><c:out value="${actionBean.trip.dateTo}"/></td>
                    </tr>
                    <tr>
                        <td><f:message key="guest.trip.availableTrips"/></td>
                        <td><c:out value="${actionBean.trip.availableTrips}"/></td>
                    </tr>
                    <tr>
                        <td><f:message key="guest.reservation.numberOfAttendants"/></td>
                        <td><s:text id="reservation-availableTrips" name="reservation.numberOfAttendants"/></td>
                    </tr>
                    <tr>
                        <th></th>
                        <td><s:submit name="makeReservation"><f:message key="guest.makeReservation"/></s:submit></td>
                    </tr>
                </tbody>
            </table>          
        
            <s:hidden name="trip.id" value="${trip.id}"/>
            
            
        </s:form>
    </s:layout-component>
        
     
</s:layout-render>