<%-- 
    Document   : listAvailableTrips
    Created on : 21-Nov-2013, 00:27:35
    Author     : infi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<s:errors/>

<s:layout-render name="/layout.jsp" titlekey="admin.excursion.title">
    <s:layout-component name="head">
        
    </s:layout-component>
    
    <s:layout-component name="menu">
        <li><s:link event="index" beanclass="cz.infi.javatravelagency.GuestTripActionBean"><f:message key="admin.index.menu.about"/></s:link></li>  
        <li><a href="<c:url value="/j_spring_security_logout" />"><f:message key="admin.index.menu.logout"/></a></li> 
    </s:layout-component>

    
    <s:layout-component name="body">
        <h2><f:message key="guest.reservationDetail"/></h2>
        <br style="clear: both;"/>
       
        <table class="table">
            <tbody>
                <tr>
                    <td><f:message key="guest.reservation.numberOfAttendants"/></td>
                    <td><c:out value="${actionBean.reservation.numberOfAttendants}"/></td>
                </tr>
                <tr>
                    <td><f:message key="guest.reservation.date"/></td>
                    <td><c:out value="${actionBean.reservation.date}"/></td>
                </tr>
            </tbody>
        </table>
        <c:if test="${actionBean.reservation.canceled}">
            <span style="color: red;">Canceled</span>
        </c:if>
        <c:if test="${!actionBean.reservation.canceled}">
            <s:form beanclass="cz.infi.javatravelagency.GuestReservationActionBean">
                <s:hidden name="reservation.id" value="${actionBean.reservation.id}"/>
                <s:submit name="cancel"><f:message key="cancel"/></s:submit>
            </s:form> 
        </c:if>
  
                
        <h3><f:message key="guest.reservation.assignedExcursions"/></h3>
        <table class="table">
            <thead>
                    <tr>
                        <th><f:message key="excursion.list.id"/></th>
                        <th><f:message key="excursion.list.date"/></th>
                        <th><f:message key="excursion.list.duration"/></th>
                        <th><f:message key="excursion.list.description"/></th>
                        <th><f:message key="excursion.list.destination"/></th>
                        <th><f:message key="excursion.list.price"/></th>
                        <th></th>
                    </tr>
            </thead>
            <tbody>                              
                    <c:forEach items="${actionBean.reservation.excursions}" var="excursion"> 
                        <tr>
                            <td>${excursion.id}</td>
                            <td><c:out value="${excursion.excursionDate}"/></td>
                            <td><c:out value="${excursion.duration}"/></td>
                            <td><c:out value="${excursion.description}"/></td>
                            <td><c:out value="${excursion.destination}"/></td>
                            <td><c:out value="${excursion.price}"/></td>
                            <td></td>
                        </tr>
                    </c:forEach>
            </tbody> 
        </table>
            
        <s:form beanclass="cz.infi.javatravelagency.GuestReservationActionBean">
            <s:hidden name="reservation.id" value="${reservation.id}"/>
            <s:select name="excursion.id">  
                <s:options-collection collection="${actionBean.availableExcursions}" value="id" label="destination" />
            </s:select> 
            <s:submit name="addExcursion"><f:message key="guest.reservation.addExcursion"/></s:submit>
        </s:form>            
                               
    </s:layout-component>
        
     
</s:layout-render>