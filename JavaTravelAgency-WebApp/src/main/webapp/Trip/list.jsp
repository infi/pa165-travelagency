<%-- 
    Document   : trip
    Created on : 22.11.2013, 13:19:23
    Author     : scotty
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>

<s:layout-render name="/layout.jsp" titlekey="admin.trip.title">
    <s:layout-component name="head">
        
    </s:layout-component>
    
    <s:layout-component name="menu">
        <li><s:link event="index"     beanclass="cz.infi.javatravelagency.AdminActionBean"><f:message key="admin.index.menu.about"/></s:link></li>
        <li><s:link event="customers" beanclass="cz.infi.javatravelagency.AdminActionBean"><f:message key="admin.index.menu.customers"/></s:link></li>
        <li><s:link event="excursions" beanclass="cz.infi.javatravelagency.AdminActionBean"><f:message key="admin.index.menu.excursions"/></s:link></li>
        <li><s:link event="trips" class="active"     beanclass="cz.infi.javatravelagency.AdminActionBean"><f:message key="admin.index.menu.trips"/></s:link></li>
        <li><s:link event="reservations" beanclass="cz.infi.javatravelagency.AdminActionBean"><f:message key="admin.index.menu.reservations"/></s:link></li>
        <li><a href="<c:url value="/j_spring_security_logout" />"><f:message key="admin.index.menu.logout"/></a></li>                         
    </s:layout-component>

    
    <s:layout-component name="body">
        <s:form beanclass="cz.infi.javatravelagency.AdminActionBean">
            <fieldset><legend><f:message key="admin.trip.title"/></legend>
                <table class="table">
                   <thead>
                       <tr>
                           <th> <f:message key="trip.list.id"/> </th>
                           <th> <f:message key="trip.list.destination"/> </th>
                           <th> <f:message key="trip.list.datefrom"/> </th>
                           <th> <f:message key="trip.list.dateto"/> </th>
                           <th> <f:message key="trip.list.available"/> </th>
                           <th> </th>
                           <th> </th>
                       </tr>
                   </thead>
                   <tbody>
                    <c:forEach items="${actionBean.trips}" var="trip">
                        <tr>
                            <td>${trip.id}</td>
                            <td><c:out value="${trip.destination}"/></td>
                            <td><c:out value="${trip.dateFrom}"/></td>
                            <td><c:out value="${trip.dateTo}"/></td>
                            <td><c:out value="${trip.availableTrips}"/></td>
                            <td>
                                <s:form beanclass="cz.infi.javatravelagency.AdminActionBean">
                                    <s:hidden name="trip.id" value="${trip.id}"/>
                                    <s:submit name="editTrip"><f:message key="edit"/></s:submit>
                                </s:form>
                            </td>
                            <td>
                                <s:form beanclass="cz.infi.javatravelagency.AdminActionBean">
                                    <s:hidden name="trip.id" value="${trip.id}"/>
                                    <s:submit name="deleteTrip"><f:message key="delete"/></s:submit>
                                </s:form>
                            </td>
                        </tr>
                    </c:forEach>
                   </tbody>
               </table>  
            </fieldset>
        </s:form>            
         
        <s:form beanclass="cz.infi.javatravelagency.AdminActionBean">
            <fieldset><legend><f:message key="admin.trip.list.newtrip"/></legend>
                <%@include file="form.jsp"%>
                <s:submit name="addTrip"><f:message key="create"/></s:submit>
            </fieldset>
        </s:form>
    </s:layout-component>
        
    <s:layout-component name="side">
    </s:layout-component>
     
</s:layout-render>