<%-- 
    Document   : edit
    Created on : 08-Dec-2013, 22:06:59
    Author     : infi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>

<s:layout-render name="/layout.jsp" titlekey="admin.trip.title">
    <s:layout-component name="head">
        
    </s:layout-component>
    
    <s:layout-component name="menu">
        <li><s:link event="index"     beanclass="cz.infi.javatravelagency.AdminActionBean"><f:message key="admin.index.menu.about"/></s:link></li>
        <li><s:link event="customers" beanclass="cz.infi.javatravelagency.AdminActionBean"><f:message key="admin.index.menu.customers"/></s:link></li>
        <li><s:link event="excursions" beanclass="cz.infi.javatravelagency.AdminActionBean"><f:message key="admin.index.menu.excursions"/></s:link></li>
        <li><s:link event="trips" class="active"     beanclass="cz.infi.javatravelagency.AdminActionBean"><f:message key="admin.index.menu.trips"/></s:link></li>
        <li><s:link event="reservations" beanclass="cz.infi.javatravelagency.AdminActionBean"><f:message key="admin.index.menu.reservations"/></s:link></li>
                                
    </s:layout-component>

    
    <s:layout-component name="body">           
        
        <s:form beanclass="cz.infi.javatravelagency.AdminActionBean">
            <fieldset><legend><f:message key="admin.trip.editTrip"/></legend>
                <%@include file="form.jsp"%>
                <s:hidden name="trip.id" value="${trip.id}"/> 
                <s:submit name="saveTrip"><f:message key="edit"/></s:submit>
            </fieldset>
        </s:form>
    </s:layout-component>
        
    <s:layout-component name="side">
    </s:layout-component>
     
</s:layout-render>