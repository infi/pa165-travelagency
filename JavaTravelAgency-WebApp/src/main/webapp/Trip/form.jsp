<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<s:errors/>
<table>
    <tr>
        <th><s:label for="trip-dateFrom" name="trip.list.datefrom"/></th>
        <td><s:text id="trip-dateFrom" name="trip.dateFrom"/></td>
    </tr>
    <tr>
        <th><s:label for="trip-dateTo" name="trip.list.dateto"/></th>
        <td><s:text id="trip-dateTo" name="trip.dateTo"/></td>
    </tr>
    <tr>
        <th><s:label for="trip-destination" name="trip.list.destination"/></th>
        <td><s:text id="trip-destination" name="trip.destination"/></td>
    </tr>
    <tr>
        <th><s:label for="trip-availableTrips" name="trip.list.available"/></th>
        <td><s:text id="trip-availableTrips" name="trip.availableTrips"/></td>
    </tr>
</table>
    
    <script type="text/javascript">
        var DATE_FORMAT = "<f:message key="jQueryDateFormat"/>";
        $(function() {
            $("#trip-dateFrom, #trip-dateTo").datepicker({"dateFormat": DATE_FORMAT});
        });
    </script>