<%-- 
    Document   : trip
    Created on : 22.11.2013, 13:19:23
    Author     : scotty
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>

<s:layout-render name="/layout.jsp" titlekey="admin.trip.title">
    <s:layout-component name="head">
        
    </s:layout-component>
    
    <s:layout-component name="menu">
        <li><s:link event="index"     beanclass="cz.infi.javatravelagency.AdminActionBean"><f:message key="admin.index.menu.about"/></s:link></li>
        <li><s:link event="customers" beanclass="cz.infi.javatravelagency.AdminActionBean"><f:message key="admin.index.menu.customers"/></s:link></li>
        <li><s:link event="excursions" beanclass="cz.infi.javatravelagency.AdminActionBean"><f:message key="admin.index.menu.excursions"/></s:link></li>
        <li><s:link event="trips" class="active"     beanclass="cz.infi.javatravelagency.AdminActionBean"><f:message key="admin.index.menu.trips"/></s:link></li>
        <li><s:link event="reservations" beanclass="cz.infi.javatravelagency.AdminActionBean"><f:message key="admin.index.menu.reservations"/></s:link></li>
        <li><a href="<c:url value="/j_spring_security_logout" />"><f:message key="admin.index.menu.logout"/></a></li>                        
    </s:layout-component>

    
    <s:layout-component name="body">
                   
    </s:layout-component>
        
    <s:layout-component name="side">
    </s:layout-component>
     
</s:layout-render>