<%-- 
    Document   : index
    Created on : 17.11.2013, 22:49:06
    Author     : scotty
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<s:layout-render name="/layout.jsp" titlekey="index.title">
    <s:layout-component name="menu">

                          
    </s:layout-component>
    
    <s:layout-component name="body">
        <h2><f:message key ="index.body.login"/></h2>
       
	<c:if test="${not empty error}">
		<div class="errorblock">
			Your login attempt was not successful, try again.<br /> Caused :
			${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
		</div>
	</c:if>
        
        <form name='f' action="<c:url value='j_spring_security_check' />"
		method='POST'>
 
		<table>
			<tr>
				<td><f:message key="index.body.login.user"/></td>
				<td><input type='text' name='j_username' value=''>
				</td>
			</tr>
			<tr>
				<td><f:message key="index.body.login.password"/></td>
				<td><input type='password' name='j_password' />
				</td>
			</tr>
			<tr>
				<td colspan='2'><input name="submit" type="submit"
					value=<f:message key="index.body.login.submit"/> />
				</td>
			</tr>
                        <tr>
                             <td><s:link event="create" beanclass="cz.infi.javatravelagency.RegistrationActionBean"><f:message key="index.body.registration"/></s:link></td>
                        </tr>
		</table>
 
	</form>
    </s:layout-component>
    
    <s:layout-component name="side">
    </s:layout-component>
    
</s:layout-render>