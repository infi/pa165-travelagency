<%-- 
    Document   : loginfailed
    Created on : 18.1.2014, 20:26:09
    Author     : Miloš Skoták
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<s:layout-render name="/layout.jsp" titlekey="index.title">
    <s:layout-component name="menu">

                          
    </s:layout-component>
    
    <s:layout-component name="body">
        <h2><f:message key ="errors.loginfailed.title"/></h2>
        <p><f:message key ="errors.loginfailed.text"/></p>
    </s:layout-component>
    
    <s:layout-component name="side">
    </s:layout-component>
    
</s:layout-render>

