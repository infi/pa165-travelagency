<%-- 
    Document   : reservation
    Created on : 22.11.2013, 13:22:36
    Author     : infi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>

<s:layout-render name="/layout.jsp" titlekey="admin.reservation.title">
    <s:layout-component name="head">
        
    </s:layout-component>
    
    <s:layout-component name="menu">
        <li><s:link event="index"     beanclass="cz.infi.javatravelagency.AdminActionBean"><f:message key="admin.index.menu.about"/></s:link></li>
        <li><s:link event="customers" beanclass="cz.infi.javatravelagency.AdminActionBean"><f:message key="admin.index.menu.customers"/></s:link></li>
        <li><s:link event="excursions" beanclass="cz.infi.javatravelagency.AdminActionBean"><f:message key="admin.index.menu.excursions"/></s:link></li>
        <li><s:link event="trips"     beanclass="cz.infi.javatravelagency.AdminActionBean"><f:message key="admin.index.menu.trips"/></s:link></li>
        <li><s:link event="reservations" class="active" beanclass="cz.infi.javatravelagency.AdminActionBean"><f:message key="admin.index.menu.reservations"/></s:link></li>
                                
    </s:layout-component>

    
    <s:layout-component name="body">
       <h2><f:message key="guest.reservationDetail"/></h2>
        <br style="clear: both;"/>
       
        <table class="table">
            <tbody>
                <tr>
                    <td><f:message key="guest.reservation.numberOfAttendants"/></td>
                    <td><c:out value="${actionBean.reservation.numberOfAttendants}"/></td>
                </tr>
                <tr>
                    <td><f:message key="guest.reservation.date"/></td>
                    <td><c:out value="${actionBean.reservation.date}"/></td>
                </tr>
                <tr>
                    <td><f:message key="guest.reservation.customer"/></td>
                    <td><c:out value="${actionBean.reservation.customer.username}"/></td>
                </tr>
            </tbody>
        </table>          

        <h3><f:message key="guest.reservation.assignedExcursions"/></h3>
        <table class="table">
            <thead>
                    <tr>
                        <th><f:message key="excursion.list.id"/></th>
                        <th><f:message key="excursion.list.date"/></th>
                        <th><f:message key="excursion.list.duration"/></th>
                        <th><f:message key="excursion.list.description"/></th>
                        <th><f:message key="excursion.list.destination"/></th>
                        <th><f:message key="excursion.list.price"/></th>
                        <th></th>
                    </tr>
            </thead>
            <tbody>                              
                    <c:forEach items="${actionBean.reservation.excursions}" var="excursion"> 
                        <tr>
                            <td>${excursion.id}</td>
                            <td><c:out value="${excursion.excursionDate}"/></td>
                            <td><c:out value="${excursion.duration}"/></td>
                            <td><c:out value="${excursion.description}"/></td>
                            <td><c:out value="${excursion.destination}"/></td>
                            <td><c:out value="${excursion.price}"/></td>
                            <td></td>
                        </tr>
                    </c:forEach>
            </tbody> 
        </table>
            
        <s:form beanclass="cz.infi.javatravelagency.AdminActionBean">
            <s:hidden name="reservation.id" value="${reservation.id}"/>
            <s:select name="excursion.id">  
                <s:options-collection collection="${actionBean.availableExcursions}" value="id" label="destination" />
            </s:select> 
            <s:submit name="reservationAddExcursion"><f:message key="guest.reservation.addExcursion"/></s:submit>
        </s:form>            
                                     
    </s:layout-component>
        
    <s:layout-component name="side">
    </s:layout-component>
     
</s:layout-render>