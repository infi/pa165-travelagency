<%-- 
    Document   : listReservation
    Created on : 22.11.2013, 15:14:25
    Author     : scotty
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<s:errors/>
<s:useActionBean beanclass="cz.infi.javatravelagency.AdminActionBean" var="actionBean"/>
<table class="table">
    <thead>
            <tr>
                <th><f:message key="reservation.listofreservation.id"/></th>
                <th><f:message key="reservation.listofreservation.date"/></th>
                <th><f:message key="reservation.listofreservation.trip"/></th>
                <th><f:message key="reservation.listofreservation.excursions"/></th>
                <th> </th>
            </tr>
    </thead>
    <tbody>         
        <c:forEach items="${actionBean.reservations}" var="reservation">
            <tr>
                <td>${reservation.id}</td>
                <td><c:out value="${reservation.date}"/></td>
                <td><c:out value="${reservation.trip.destination}"/></td>
                <td>
                    <c:forEach items="${reservation.excursions}" var="excursion" varStatus="stat"><c:if test="${!stat.first}">, </c:if>${excursion.destination}</c:forEach>    
                </td>
                
                <td>
                    <s:form beanclass="cz.infi.javatravelagency.AdminActionBean">
                        <s:hidden name="reservation.id" value="${reservation.id}"/>
                        <s:submit name="deleteReservation"><f:message key="delete"/></s:submit>
                    </s:form>
                </td>
                <td>
                    <s:form beanclass="cz.infi.javatravelagency.AdminActionBean">
                        <s:hidden name="reservation.id" value="${reservation.id}"/>
                        <s:submit name="reservationDetail"><f:message key="detail"/></s:submit>
                    </s:form>
                </td>
            </tr>
        </c:forEach>
    </tbody> 
</table>
