<%-- 
    Document   : create
    Created on : 18.1.2014, 18:10:53
    Author     : Skoták Miloš
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<s:layout-render name="/layout.jsp" titlekey="registration.title">
    <s:layout-component name="menu">

                          
    </s:layout-component>
    
    <s:layout-component name="body">
        <h2><f:message key ="registration.body.registration"/></h2>
        <s:form beanclass="cz.infi.javatravelagency.RegistrationActionBean">
            <fieldset>
                <%@include file="/Customer/formCustomers.jsp"%>
                <s:submit name="addCustomer"><f:message key="create"/></s:submit>
            </fieldset>
        </s:form>
       
	
    </s:layout-component>
    
    <s:layout-component name="side">
    </s:layout-component>
    
</s:layout-render>
