<%-- 
    Document   : formExcursion
    Created on : 27.11.2013, 0:26:38
    Author     : scotty
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<s:errors/>
<table>
    <tr>                               
        <th><f:message key="excursion.date"/></th>
        <td><s:text id="excursion-excursionDate" name="excursion.excursionDate"/></td>
    </tr>
    <tr>
        <th><f:message key="excursion.duration"/></th>
        <td><s:text id="excursion-duration" name="excursion.duration"/></td>
    </tr>
    <tr>
        <th><f:message key="excursion.description"/></th>
        <td><s:text id="excursion-description" name="excursion.description"/></td>
    </tr>
    <tr>
        <th><f:message key="excursion.destination"/></th>
        <td><s:text id="excursion-destination" name="excursion.destination"/></td>
    </tr>
    <tr>
        <th><f:message key="excursion.price"/></th>
        <td><s:text id="excursion-price" name="excursion.price"/></td>
    </tr>
    <tr>
        <th><f:message key="excursion.trip"/></th>
        <td>
            <s:select name="trip.id">  
                <s:options-collection collection="${actionBean.trips}" value="id" label="destination" />
            </s:select> 
        </td>
    </tr>
    
</table>
    
    <script type="text/javascript">
        var DATE_FORMAT = "<f:message key="jQueryDateFormat"/>";
        $(function() {
            $("#excursion-excursionDate").datepicker({ dateFormat: DATE_FORMAT});
        });
    </script>
