<%-- 
    Document   : list
    Created on : 26.11.2013, 22:58:54
    Author     : scotty
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<s:useActionBean beanclass="cz.infi.javatravelagency.AdminActionBean" var="actionBean"/>
<table class="table">
    <thead>
            <tr>
                <th><f:message key="excursion.list.id"/></th>
                <th><f:message key="excursion.list.date"/></th>
                <th><f:message key="excursion.list.duration"/></th>
                <th><f:message key="excursion.list.description"/></th>
                <th><f:message key="excursion.list.destination"/></th>
                <th><f:message key="excursion.list.price"/></th>
                <th></th>
                <th></th>
            </tr>
    </thead>
    <tbody>                              
            <c:forEach items="${actionBean.excursions}" var="excursion"> 
                <tr>
                    <td>${excursion.id}</td>
                    <td><c:out value="${excursion.excursionDate}"/></td>
                    <td><c:out value="${excursion.duration}"/></td>
                    <td><c:out value="${excursion.description}"/></td>
                    <td><c:out value="${excursion.destination}"/></td>
                    <td><c:out value="${excursion.price}"/></td>
                    <td>
                        <s:form beanclass="cz.infi.javatravelagency.AdminActionBean">
                            <s:hidden name="excursion.id" value="${excursion.id}"/>
                            <s:submit name="editExcursion"><f:message key="edit"/></s:submit>
                        </s:form>
                    </td>
                    <td>
                        <s:form beanclass="cz.infi.javatravelagency.AdminActionBean">
                            <s:hidden name="excursion.id" value="${excursion.id}"/>
                            <s:submit name="deleteExcursion"><f:message key="delete"/></s:submit>
                        </s:form>
                    </td>
                </tr>
            </c:forEach>
     </tbody> 
</table>