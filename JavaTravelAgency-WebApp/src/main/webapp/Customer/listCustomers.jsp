<%-- 
    Document   : listCustomers
    Created on : 22.11.2013, 13:40:27
    Author     : scotty
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<s:useActionBean beanclass="cz.infi.javatravelagency.AdminActionBean" var="actionBean"/>
<table class="table">
    <thead>
            <tr>
                <th><f:message key="customer.listofcustomers.id"/></th>
                <th><f:message key="customer.listofcustomers.firstname"/></th>
                <th><f:message key="customer.listofcustomers.surname"/></th>
                <th><f:message key="customer.listofcustomers.contacts"/></th>
                <th><f:message key="customer.listofcustomers.address"/></th>
                <th><f:message key="customer.listofcustomers.phone"/></th>
            </tr>
    </thead>
    <tbody>    
            <c:forEach items="${actionBean.customers}" var="customer">
                <tr>
                    <td>${customer.id}</td>
                    <td><c:out value="${customer.name}"/></td>
                    <td><c:out value="${customer.surname}"/></td>
                    <td><c:out value="${customer.contacts}"/></td>
                    <td><c:out value="${customer.address}"/></td>
                    <td><c:out value="${customer.phone}"/></td>
                    <td>
                        <s:form beanclass="cz.infi.javatravelagency.AdminActionBean">
                            <s:hidden name="customer.id" value="${customer.id}"/>
                            <s:submit name="editCustomer"><f:message key="edit"/></s:submit>
                        </s:form>
                    </td>
                    <td>
                        <s:form beanclass="cz.infi.javatravelagency.AdminActionBean">
                            <s:hidden name="customer.id" value="${customer.id}"/>
                            <s:submit name="deleteCustomer"><f:message key="delete"/></s:submit>
                        </s:form>
                    </td>
                </tr>
            </c:forEach>
    </tbody> 
</table>
