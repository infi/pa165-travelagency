<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<s:errors/>
<table>
    <tr>                               
        <th><f:message key="customer.name"/></th>
        <td><s:text id="customer-name" name="customer.name"/></td>
    </tr>
    <tr>
        <th><f:message key="customer.surname"/></th>
        <td><s:text id="customer-surname" name="customer.surname"/></td>
    </tr>
    <tr>
        <th><f:message key="customer.username"/></th>
        <td><s:text id="customer-username" name="customer.username"/></td>
    </tr>
    <tr>
        <th><f:message key="customer.password"/></th>
        <td><s:password id="customer-password" name="customer.password"/></td>
    </tr>
    <tr>
        <th><f:message key="customer.contacts"/></th>
        <td><s:text id="customer-contacts" name="customer.contacts"/></td>
    </tr>
    <tr>
        <th><f:message key="customer.address"/></th>
        <td><s:text id="customer-address" name="customer.address"/></td>
    </tr>
    <tr>
        <th><f:message key="customer.phone"/></th>
        <td><s:text id="customer-phone" name="customer.phone"/></td>
    </tr>
    <s:hidden id="customer-userrole" name="customer.userrole" value="ROLE_USER" />
</table>
    
