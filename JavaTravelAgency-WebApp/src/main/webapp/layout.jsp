<%-- 
    Document   : layout
    Created on : 18.11.2013, 23:58:30
    Author     : scotty
--%>

<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<s:layout-definition>
<!DOCTYPE html>
<html>
    <head>
        <title><f:message key="${titlekey}"/></title>
        <meta charset="utf-8">
        
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/reset.css" type="text/css" media="screen">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css" media="screen">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/grid.css" type="text/css" media="screen">   
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui-1.10.3.custom.css" type="text/css" media="screen">   
        
        <script src="${pageContext.request.contextPath}/js/jquery-1.9.1.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/FF-cash.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/script.js" type="text/javascript"></script>
        <s:layout-component name="header"/>
    </head>
    
    <body onload='document.f.j_username.focus();'>
       
    <!--==============================header=================================-->
    <header>
        
    	<div class="main">
            <div class="wrapper">
            <h1><a href="/pa165/"><f:message key="index.logo.eaglestroops"/></a></h1>
                <div class="user-menu">
                    <c:if test="${not empty actionBean.username}">
                        ${actionBean.username}
                    </c:if>
                </div> 
                <nav class="fright">
                    <ul class="menu">
                        <s:layout-component name="menu"/>
                    </ul>
                </nav>
            </div>  
        </div>
        
    </header>
    
    <!--==============================content================================-->
    <section id="content">
        <div class="ic">More Website Templates @ TemplateMonster.com - February 06, 2012!</div>
        <div class="main">
            <div class="container_12">
                <div class="wrapper">
                    <article class="grid_8">
                        <s:messages/>
                        <s:layout-component name="body"/>
                    </article>
                                              
                    <article class="grid_4">
                    	<div class="indent-top">
                            <s:layout-component name="side"/>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>
    
    <!--==============================footer=================================-->
    <footer>
        <div class="main">
        	<div class="container_12">	
            	<div class="wrapper">
                    <div class="grid_8">
                        <div class="aligncenter">
                            Java pa165<br>
                        </div>
                    </div>
                    <div class="grid_3 prefix_1">

                    </div>
                </div>
            </div>
        </div>
    </footer>
    </body>
</html>
</s:layout-definition>
